#include <QObject>

class MyObj : public QObject
{
public:
    using QObject::QObject;
};

int main()
{
    new MyObj(nullptr);
}
