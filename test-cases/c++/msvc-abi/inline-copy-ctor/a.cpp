#ifndef EXPORT_MACRO
# define EXPORT_MACRO __declspec(dllexport)
#endif


struct EXPORT_MACRO Foo1 {
};

struct EXPORT_MACRO Foo2 {
    Foo2() {}
};

struct EXPORT_MACRO Foo3 {
    Foo3() {}
    Foo3(const Foo3 &) {}
};


struct EXPORT_MACRO Foo4 {
    Foo4() {}
    Foo4(const Foo4 &) = default;
};

struct EXPORT_MACRO Foo5 {
    Foo5() {}
    Foo5(const Foo5 &) = delete;
};


struct EXPORT_MACRO Foo6 {
    Foo6() {}
    ~Foo6() {}
};

/***
 *        1    0 00001000 public: __cdecl Foo2::Foo2(void) __ptr64
          2    1 00001000 public: __cdecl Foo3::Foo3(struct Foo3 const & __ptr64) __ptr64
          3    2 00001000 public: __cdecl Foo3::Foo3(void) __ptr64
          4    3 00001000 public: __cdecl Foo4::Foo4(void) __ptr64
          5    4 00001000 public: __cdecl Foo5::Foo5(void) __ptr64
          6    5 00001000 public: struct Foo1 & __ptr64 __cdecl Foo1::operator=(struct Foo1 && __ptr64) __ptr64
          7    6 00001000 public: struct Foo1 & __ptr64 __cdecl Foo1::operator=(struct Foo1 const & __ptr64) __ptr64
          8    7 00001000 public: struct Foo2 & __ptr64 __cdecl Foo2::operator=(struct Foo2 && __ptr64) __ptr64
          9    8 00001000 public: struct Foo2 & __ptr64 __cdecl Foo2::operator=(struct Foo2 const & __ptr64) __ptr64
         10    9 00001000 public: struct Foo3 & __ptr64 __cdecl Foo3::operator=(struct Foo3 const & __ptr64) __ptr64
         11    A 00001000 public: struct Foo4 & __ptr64 __cdecl Foo4::operator=(struct Foo4 const & __ptr64) __ptr64
         12    B 00001000 public: struct Foo5 & __ptr64 __cdecl Foo5::operator=(struct Foo5 const & __ptr64) __ptr64
*/