#include "a.cpp"
#include <QDebug>
#include <QBasicTimer>

#include <iostream>

int main()
{
    std::cout << "QBasicTimer: " << std::is_trivially_copyable<QBasicTimer>::value << std::endl;

    std::cout << "Foo1: " << std::is_trivially_copyable<Foo1>::value << std::endl;
    std::cout << "Foo2: " << std::is_trivially_copyable<Foo2>::value << std::endl;
    std::cout << "Foo3: " << std::is_trivially_copyable<Foo3>::value << std::endl;
    std::cout << "Foo4: " << std::is_trivially_copyable<Foo4>::value << std::endl;
    std::cout << "Foo5: " << std::is_trivially_copyable<Foo5>::value << std::endl;
    std::cout << "Foo6: " << std::is_trivially_copyable<Foo6>::value << std::endl;

    std::cout << "QBasicTimer assign-copyiable?: " << std::is_trivially_copy_assignable<QBasicTimer>::value << std::endl;

    return 0;
}