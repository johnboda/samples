
#include <iostream>

struct Struct {
    void method();
};

int main()
{
    static auto pointer1 = &Struct::method;
    auto pointer2 = &Struct::method;

    std::cout << (pointer1 == &Struct::method) << "; " << (pointer2 == &Struct::method) << "\n";
    return 0;
}
