#include <iostream>

struct MyStruct
{
    MyStruct(const char*)
    {
        std::cout << "MyStruct(const char*)\n";
    }
};


void overload(bool)
{
    std::cout << "overload(bool)\n";
}

void overload(MyStruct)
{
    std::cout << "overload(MyStruct)\n";
}

int main()
{
    overload("test");
    return 0;
}