#include <vector>

struct A
{
    A() {}
    A(const A &) = delete;
    A(const A &&) {}
};

int main()
{
    std::vector<A> v;
    return 0;
}