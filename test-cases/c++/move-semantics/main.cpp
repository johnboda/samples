#include <iostream>

struct A
{
    A()
    {
        std::cout << "A\n";
    }
    ~A()
    {
        std::cout << "~A\n";
    }
    A(const A&)
    {
        std::cout << "A-copy\n";
    }
    A(const A &&)
    {
        std::cout << "A-move\n";
    }

    int getX() const &
    {
        return 1;
    }

    int getX() const && = delete;

};

const A getConstA()
{
    return A();
}

A getA()
{
    return A();
}
void foo(const A&)
{
    std::cout << "foo\n";
}

void foo(A&&)
{
    std::cout << "foo &&\n";
}

void foo(const A&&)
{
    std::cout << "foo const-&&\n";
}

int main()
{
    //foo(getConstA());
    //foo(getA());

    //A a(getConstA());
    A a;
    //std::cout << A().getX() << "\n";
    std::cout << a.getX() << "\n";

}
