#include <iostream>

struct A
{
};

struct A1
{
    ~A1() {}
};

struct A2
{
    ~A2() = default;
};

int main()
{
    std::cout << std::is_copy_constructible<A>::value << "\n";
    std::cout << std::is_copy_constructible<A1>::value << "\n";
    std::cout << std::is_trivially_copy_constructible<A1>::value << "\n";
    std::cout << std::is_trivially_copy_constructible<A2>::value << "\n";

    std::cout << std::is_move_constructible<A1>::value << "\n";
    std::cout << std::is_move_constructible<A2>::value << "\n";


    return 0;
}
