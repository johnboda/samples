#include <iostream>
#include <vector>

using namespace std;

struct MyStruct {
    MyStruct()
    {
        cout << "CTOR\n";
    }

    ~MyStruct()
    {
        cout << "DTOR\n";
    }

    MyStruct(const MyStruct &)
    {
        cout << "COPY-CTOR\n";
    }

    MyStruct(const MyStruct &&)
    {
        cout << "MOVE-CTOR\n";
    }
};

int main()
{
    vector<MyStruct> vec;
    vector<MyStruct> vec2;
    vec.push_back(MyStruct());
    cout << "start:\n";

    for (const auto &s : vec) {
        vec2.push_back(s);
    }
}
