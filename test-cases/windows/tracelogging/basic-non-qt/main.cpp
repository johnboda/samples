#pragma once

#include <windows.h>
#include <TraceLoggingProvider.h>

TRACELOGGING_DECLARE_PROVIDER(g_myProvider);

TRACELOGGING_DEFINE_PROVIDER(
    g_myProvider,
    "SimpleTraceLoggingProvider",
    (0x3970f9cf, 0x2c0c, 0x4f11, 0xb1, 0xcc, 0xe3, 0xa1, 0xe9, 0x95, 0x88, 0x33));

int main()
{

    TraceLoggingRegister(g_myProvider);

    // Log an event
    TraceLoggingWrite(g_myProvider,
        "HelloWorldTestEvent",              // Event Name that should uniquely identify your event.
        TraceLoggingValue(111, "TestMessage")); // Field for your event in the form of (value, field name).

    // Stop TraceLogging and unregister the provider
    TraceLoggingUnregister(g_myProvider);

    return 0;
}