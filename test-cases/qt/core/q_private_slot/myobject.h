#include <QObject>

class MyObj : public QObject {
Q_OBJECT
public:
    MyObj() : d(new Private())
    {

    }

    void test()
    {
        QObject o;
        connect(&o, SIGNAL(destroyed()), this, SLOT(_q_my_private_slot()));
    }

signals:
    void _q_my_private_slot();

private:

    class Private
    {
    public:
        void _q_my_private_slot() {}
    };

    Private *const d;
    Private* d_func() { return d; }
    Q_PRIVATE_SLOT(d_func(), void _q_my_private_slot())
};
