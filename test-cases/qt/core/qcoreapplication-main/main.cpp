#include <QDebug>
#include <QCoreApplication>

class Obj : public QObject
{
public:
    Obj()
    {
        qApp->installEventFilter(this);
    }

    ~Obj()
    {

    }

    bool eventFilter(QObject *o, QEvent *e) override
    {
        qDebug() << o << e;

        return false;
    }
};

int main (int a, char **b)
{
    QCoreApplication app(a, b);

    Obj o;

    return app.exec();
}
