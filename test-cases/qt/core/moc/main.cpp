#include <QDebug>
#include <QCoreApplication>
#include <QSharedPointer>

class MyCustom { int a; };

class Obj : public QObject
{
    Q_OBJECT
    Q_PROPERTY(MyCustom mycustom READ mycustom CONSTANT)
public:
    Obj()
    {
        qApp->installEventFilter(this);
    }

    ~Obj()
    {

    }

    bool eventFilter(QObject *o, QEvent *e) override
    {
        qDebug() << o << e;

        return false;
    }

    MyCustom mycustom() const { return MyCustom(); }

public Q_SLOTS:
    void method(MyCustom){}
    void method2(QSharedPointer<MyCustom>) {}
signals:
    void sig1(MyCustom);
    void sig2(QSharedPointer<MyCustom>);
};

int main (int a, char **b)
{
    QCoreApplication app(a, b);

    Obj o;

    return app.exec();
}
