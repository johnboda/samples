#include <QCoreApplication>
#include <QTextStream>
#include <QTextCodec>
#include <QDebug>
#include <QMessageBox>
#include <QApplication>

int main(int a, char **b)
{
    QApplication app(a, b);

    qDebug() << QTextCodec::codecForLocale()->name();

    QTextStream stream(stdin, QIODevice::ReadOnly);

    QString line = stream.readLine();

    QMessageBox::warning(nullptr, "foo", line);

    return 0;
}

