#include <QCoreApplication>
#include <QSqlDatabase>
#include <QSqlDriver>
#include <QTest>

class PmfConnectTest : public QObject
{
    Q_OBJECT

private slots:
    void testPmfVariableStatic()
    {
        static auto od = &QObject::destroyed;
        QCOMPARE(od, &QObject::destroyed);
    }

};

QTEST_MAIN(PmfConnectTest)

#include "tst_pmfconnect.moc"