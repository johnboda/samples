#include <QtWidgets>

class TestObj : public QObject
{
    Q_OBJECT

public:
    void fire()
    {
        qDebug() << "emit defaultTrueSignal()";
        emit defaultTrueSignal();

        qDebug() << "emit defaultTrueSignal(true);";
        emit defaultTrueSignal(true);

        qDebug() << "emit defaultTrueSignal(false);";
        emit defaultTrueSignal(false);

        qDebug() << "emit defaultFalseSignal();";
        emit defaultFalseSignal();

        qDebug() << "emit defaultFalseSignal(false);";
        emit defaultFalseSignal(false);

        qDebug() << "emit defaultFalseSignal(true);";
        emit defaultFalseSignal(true);
    }

signals:
    void defaultTrueSignal(bool v = true);
    void defaultFalseSignal(bool v = false);

public slots:
    void defaultTrueSlot(bool v = true)
    {
        qDebug() << "defaultTrueSlot:" << v;
    }

    void defaultFalseSlot(bool v = false)
    {
        qDebug() << "defaultFalseSlot:" << v;
    }
};

static  void testPMFs()
{
    TestObj pmfsTestDefaultTrueSignal;

    QObject::connect(&pmfsTestDefaultTrueSignal, &TestObj::defaultTrueSignal,
                     &pmfsTestDefaultTrueSignal, &TestObj::defaultTrueSlot);
    QObject::connect(&pmfsTestDefaultTrueSignal, &TestObj::defaultTrueSignal,
                     &pmfsTestDefaultTrueSignal, &TestObj::defaultFalseSlot);

    TestObj pmfsTestDefaultFalseSignal;
    QObject::connect(&pmfsTestDefaultFalseSignal, &TestObj::defaultFalseSignal,
                     &pmfsTestDefaultFalseSignal, &TestObj::defaultTrueSlot);
    QObject::connect(&pmfsTestDefaultFalseSignal, &TestObj::defaultFalseSignal,
                     &pmfsTestDefaultFalseSignal, &TestObj::defaultFalseSlot);

    pmfsTestDefaultTrueSignal.fire();
    pmfsTestDefaultFalseSignal.fire();
}

static void testOldStyle()
{
    TestObj oldStyleTestDefaultTrueSignal;
    QObject::connect(&oldStyleTestDefaultTrueSignal, SIGNAL(defaultTrueSignal()),
                     &oldStyleTestDefaultTrueSignal, SLOT(defaultFalseSlot()));
    QObject::connect(&oldStyleTestDefaultTrueSignal, SIGNAL(defaultTrueSignal(bool)),
                     &oldStyleTestDefaultTrueSignal, SLOT(defaultFalseSlot()));
    QObject::connect(&oldStyleTestDefaultTrueSignal, SIGNAL(defaultTrueSignal(bool)),
                     &oldStyleTestDefaultTrueSignal, SLOT(defaultFalseSlot(bool)));
    /*QObject::connect(&oldStyleTestDefaultTrueSignal, SIGNAL(defaultTrueSignal()),
                     &oldStyleTestDefaultTrueSignal, SLOT(defaultFalseSlot(bool))); QObject::connect: Incompatible sender/receiver arguments
        TestObj::defaultTrueSignal() --> TestObj::defaultFalseSlot(bool) */
    oldStyleTestDefaultTrueSignal.fire();
}


int main(int a, char **b)
{
    QApplication app(a, b);

    // testPMFs();
    testOldStyle();



    return app.exec();
}

#include <main.moc>
