#include <QString>
#include <QStringView>

bool foo1(QStringView v)
{
    return v == QLatin1String("foo");
}

bool foo2(const QString &v)
{
    return v == QLatin1String("foo");
}

int main()
{
    bool res = true;
    std::string str = "foo1";
    for (int i = 0; i < 1000; i++) {
        res = res && foo1(u"foo");
    }

    return res;
}
