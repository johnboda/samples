#include <QDebug>
#include <QString>

void toBuffer(const QString &qtString, char *buffer) // Make sure your buffer has enough size beforhand
{
    const QChar *data = qtString.unicode();
    const int size = qtString.size(); // "Hello" has 5 characters
    const int numBytes = sizeof(QChar) * size; // but 10 bytes, as each character occupies 2 bytes

    memcpy(buffer, data, numBytes);
    buffer[numBytes] = '\0'; // null terminate it.

}


QString fromBuffer(char *buffer, int numberOfCharacters) // numberOfCharacters is the size of the buffer / 2
{
    return QString((QChar*)buffer, numberOfCharacters);
}

void testFromStdString()
{
    std::string s;
    qDebug() << QString::fromStdString(s);
}

void testFromTo()
{
    QString qtString = "kdab";
    const int size = qtString.size();

    char buffer[40];
    toBuffer(qtString, buffer);

    ushort *shorts = (ushort*)buffer;
    for (int i = 0; i < size; ++i)
        qDebug() << QString("buffer character %1=0x%2").arg(i).arg(QString::number(shorts[i], /*hex*/16));


    QString fromMultiByte = fromBuffer(buffer, size);
    qDebug() << fromMultiByte;
}

int main()
{


    //testFromTo();
    testFromStdString();

    return 0;
}
