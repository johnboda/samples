#include <QDebug>
#include <QCoreApplication>
#include <QSharedMemory>
#include <QTimer>

int main (int a, char **b)
{
    QCoreApplication app(a, b);

    QSharedMemory memory("key!!");

    memory.attach(); memory.detach(); // Make sure there's no shared memory from a crashed process

    bool result = memory.attach();
    if (result) {
        qDebug() << "Attached!";
    } else {
        result = memory.create(1024);
        if (result) {
            qDebug() << "Created segment";
        } else {
            qDebug() << "Error creating segment! " << memory.error();;
        }
    }

    qDebug() << "Quitting in 5 seconds. Use Ctrl-C for something less gracious";
    auto timer = new QTimer();
    timer->connect(timer, &QTimer::timeout, &app, &QCoreApplication::quit);
    timer->start(5000);

    return app.exec();
}
