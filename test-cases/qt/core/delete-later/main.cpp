#include <QDebug>
#include <QCoreApplication>

// -shows how delete later can be blocked by the event filter

class Obj : public QObject
{
public:
    Obj()
    {
        qApp->installEventFilter(this);
        setObjectName("Obj");
    }

    ~Obj()
    {
        qDebug() << "~Obj";
    }

    bool eventFilter(QObject *o, QEvent *e) override
    {
        qDebug() << "Obj::event" << o << e->type();
        return true;
    }

    bool event(QEvent *e) override
    {
        qDebug() << "Obj::event" << e->type();;
        return QObject::event(e);
    }
};

int main (int a, char **b)
{
    QCoreApplication app(a, b);
    auto o = new Obj();
    o->deleteLater();

    return app.exec();
}
