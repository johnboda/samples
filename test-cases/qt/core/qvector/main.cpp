#include <QDebug>
#include <QVector>

#include <vector>

template <typename T>
void capacity()
{
    T vec;
    vec.reserve(150000);
    vec.resize(150000);
    vec.push_back(1);
    qDebug() << "Capacity after reserve and append" << vec.capacity();

    T vec2;
    vec2.resize(150000);
    qDebug() << "Capacity after a single resize:" << vec2.capacity();

}

void somethingElse()
{
        int a[] = {1, 2, 3, 4, 5};
    QVector<int> vec;
    vec.resize(5); // <-- resize() instead of reserve
    memcpy(vec.data(), a, sizeof(int)*5);

    qDebug() << vec;
}

int main()
{

    capacity<QVector<int> >();
    capacity<std::vector<int> >();

    return 0;
}
