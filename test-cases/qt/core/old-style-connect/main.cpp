#include <QDebug>
#include <QCoreApplication>

// Shows that signal and slot arguments should be fully qualified

class Obj : public QObject
{
    Q_OBJECT
public:

    struct MyInnerType {
        int v;
    };

    Obj()
    {
        qApp->installEventFilter(this);
        auto b = connect(this, SIGNAL(signal1(MyInnerType)), SLOT(slot1(MyInnerType)));
        if (!b)
            qWarning() << "Couldn't connect";
    }

    ~Obj()
    {

    }

    void emitIt()
    {
        emit signal1(MyInnerType());
    }

    bool eventFilter(QObject *o, QEvent *e) override
    {
        qDebug() << o << e;

        return false;
    }
signals:
    void signal1(MyInnerType t);
public Q_SLOTS:
    void slot1(Obj::MyInnerType t)
    {
        qWarning() << "slot1() called";
    }
};

int main (int a, char **b)
{
    QCoreApplication app(a, b);

    Obj o;
    o.emitIt();

    return app.exec();
}

#include "main.moc"
