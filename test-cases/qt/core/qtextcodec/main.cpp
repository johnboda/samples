#include <QCoreApplication>
#include <QTextCodec>
#include <QTime>
#include <QDebug>
#include <string>
#include <iostream>

int main()
{
    std::string text = "�";
    QByteArray encodedString = text.c_str();

    QTextCodec *codec = QTextCodec::codecForName("ISO-8859-1");

    QTime t;
    QString string = codec->toUnicode(encodedString);
    std::cout << "now:" << std::string(string.toUtf8().constData()) << t.elapsed()<< "\n";
}
