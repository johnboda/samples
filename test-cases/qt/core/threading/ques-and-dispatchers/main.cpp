#include <QCoreApplication>
#include <QThread>
#include <QAbstractEventDispatcher>
#include <QDebug>

class MyEvent : public QEvent
{
public:
     MyEvent() : QEvent((QEvent::Type)(QEvent::User + 1)) {}
};

class MyThread : public QThread
{
public:
    void run() override
    {
        qDebug() << "Foo " << QAbstractEventDispatcher::instance(this);
         qApp->postEvent(this, new MyEvent()); // posts to main thread
    }

    bool event(QEvent *e) override
     {
         // Runs on the main thread!
         if (e->type() == QEvent::User + 1) {
             qDebug() << "Got it";

        }
        return false;
     }

};

int main(int a, char **b)
{
    QCoreApplication app(a, b);

    auto thread = new MyThread();
    thread->start();

    return app.exec();
}
