#include <QDebug>
#include <QCoreApplication>
#include <QThread>

#include <chrono>

class Worker : public QObject
{
public:
    void doWork() {
        qDebug() << "Doing work in thread:" << thread();;
        std::this_thread::sleep_for(std::chrono::seconds(5));


        QObject o;
        qDebug() << "New object, created on thread2 belongs to thread" << o.thread();
        qDebug() << "Done work";
    }
signals:
};

int main (int a, char **b)
{
    QCoreApplication app(a, b);
    qDebug() << "Main thread:" << app.thread();

    auto t = new QThread();
    auto worker = new Worker();
    worker->moveToThread(t);
    QObject::connect(t, &QThread::started, worker, &Worker::doWork);
    t->start();


    std::this_thread::sleep_for(std::chrono::seconds(10));
    qDebug() << "Starting qAppExec";
    return app.exec();
}
