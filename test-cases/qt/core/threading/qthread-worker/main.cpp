#include <QDebug>
#include <QCoreApplication>
#include <QThread>

#include <unistd.h>

class Worker : public QObject
{
public:
    void doWork() {
        qDebug() << "Doing work";
        sleep(5);
        qDebug() << "Done work";
    }
signals:
};

int main (int a, char **b)
{
    QCoreApplication app(a, b);

    auto t = new QThread();
    auto worker = new Worker();
    worker->moveToThread(t);
    QObject::connect(t, &QThread::started, worker, &Worker::doWork);
    t->start();


    sleep(10);
    qDebug() << "Starting qAppExec";
    return app.exec();
}
