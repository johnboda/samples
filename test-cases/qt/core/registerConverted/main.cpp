#include <QVariant>
#include <QDebug>

struct A {
    int a = 1;
};

struct B {
    int b = 1;
};

struct C {

    C() = default;
    C(A) {}

    int c = 1;
};

struct A_derived
{

};


int main()
{

    A a;
    A_derived a_derived;
    B b;
    QVariant v = QVariant::fromValue<A>(a);
    QVariant vb = QVariant::fromValue<B>(b);
    QVariant vap = QVariant::fromValue<A*>(&a);
    QVariant vaderived_p = QVariant::fromValue<A_derived*>(&a_derived);

    // true, false
    qDebug() << v.canConvert<A>() << v.canConvert<B>();

        QMetaType::registerConverter<A, B>( // A to B converted
            [](A a) -> B {
                B b;
                b.b = a.a;
                return b;
            });
    // true, true, false
    qDebug() << v.canConvert<A>() << v.canConvert<B>() << v.canConvert<C>();

    qDebug() << "Convertible? " << std::is_convertible<A, C>::value << std::is_convertible<C, A>::value;

    qDebug() << "Convertible A_derived* -> A* ?" << vaderived_p.canConvert<A*>();

    // false, true
    qDebug() << vb.canConvert<A>() << vb.canConvert<B>();

    return 0;
}

Q_DECLARE_METATYPE(A)
Q_DECLARE_METATYPE(B)
Q_DECLARE_METATYPE(C)
Q_DECLARE_METATYPE(A_derived)
Q_DECLARE_METATYPE(A*)
Q_DECLARE_METATYPE(B*)
Q_DECLARE_METATYPE(C*)
Q_DECLARE_METATYPE(A_derived*)
