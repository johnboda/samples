#include <QDebug>
#include <QCoreApplication>

#include <type_traits>

struct MyA {
    MyA() {}
};


//Q_GLOBAL_STATIC_WITH_ARGS(const QString, strUnit, QStringLiteral("unit"))

Q_GLOBAL_STATIC(MyA, s_myglobalStatic)


int main (int a, char **b)
{
    QCoreApplication app(a, b);

    qDebug() << std::is_trivially_constructible<decltype(s_myglobalStatic)>::value;

    return app.exec();
}
