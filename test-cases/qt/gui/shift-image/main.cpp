#include <QImage>
#include <QDebug>
#include <QPainter>
#include <QTime>

void shiftImage(QImage &img, int amount)
{
    QTime t;
    t.start();
    QPainter p(&img);

    const QRect rect = QRect(QPoint(0,0), img.size());
    const QRect targetRect = rect.adjusted(0, 0, -amount, 0);
    const QRect sourceRect = rect.adjusted(amount, 0, 0, 0);
    p.drawImage(targetRect, img, sourceRect);

    const QRect rectNeedingRepaint = rect.adjusted(img.width() - amount, 0, 0, 0);
    p.fillRect(rectNeedingRepaint, Qt::white); // your vertical bar will be painted in this rect "rectNeedingRepaint"
    qDebug() << "Elapsed" << t.elapsed() << "ms";
}

void shiftImage2(QImage &img, int amount)
{
    // Not working yet, but shows that memcpy is as expensive as using the QPainter
    QTime t;
    t.start();
    const int numLines = img.height();
    const size_t sourceWidth = static_cast<size_t>(img.width() - amount);
    qDebug() << numLines;
    for (int i = 0; i < numLines; i++) {
        auto line = img.scanLine(i);
        memcpy(line, line + amount, sourceWidth);
    }
    qDebug() << "Elapsed" << t.elapsed() << "ms";
}

int main()
{
    QImage img("qt.png");
    qDebug() << "qt.png: " << img.size();

    shiftImage(img, 100); // shift by 100px with QPainter
    //shiftImage2(img, 100);

    img.save("new.png");
    qDebug() << "img.png: " << img.size();

    return 0;
}
