#include <QApplication>
#include <QtWidgets>

class Widget : public QWidget
{
public:
    Widget()
    {
        resize(500, 500);
    }

    void paintEvent(QPaintEvent *) override
    {
        QPainter p(this);

        QString text = "Foo";
        QFontMetrics fm(font());
        auto r = fm.tightBoundingRect(text);
        auto r2 = fm.boundingRect(text[0]);
        auto pos = rect().center();
        r.moveBottomLeft(pos);
        r2.moveBottomLeft(pos);


        qDebug() << fm.size(Qt::TextSingleLine, text).height() << r.height();

        p.drawRect(r);
        p.drawRect(r2);

        p.drawText(pos, text);

    }
};

int main(int a, char **b)
{
    QApplication app(a, b);

    Widget w;
    w.show();

    return app.exec();
}
