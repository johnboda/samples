#include <QDebug>
#include <QRect>

int main()
{
    QRect r(0,0,100,100);
    qDebug() << r.y() << r.height() << r.bottom();

    QRect r2(0,0,2,2);
    qDebug() << r2.height() << r2.bottom();
    return 0;
}
