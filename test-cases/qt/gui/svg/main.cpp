#include <QApplication>
#include <QtWidgets>
#include <QtSvg>
#include <QImage>
#include <QDebug>

class MyWidget : public QWidget
{
public:
    explicit MyWidget(const QString &filename)
        : QWidget()
        , m_filename(filename)
    {
    }

    void paintEvent(QPaintEvent *)
    {
         QSvgRenderer renderer(m_filename);
         renderer.setViewBox(QRect(0, 0, 70, 70));
         QPainter p(this);
         renderer.render(&p);
    }

    const QString m_filename;
};


static void saveToPng(QSize size, const QString &sourceFilename, const QString &elementId)
{
    QImage image(size, QImage::Format_ARGB32_Premultiplied);
    QPainter p(&image);
    QSvgRenderer renderer(sourceFilename);
    renderer.render(&p, elementId,  QRect(QPoint(0,0), image.size()));
    image.save(QString("%1.png").arg(elementId));
}

int main(int a, char** b)
{
    QApplication app(a, b);

    if (app.arguments().size() != 2) {
        qWarning() << "Needs an argument";
        return 1;
    }


    const QString filename = app.arguments()[1];

    QSvgWidget widget;
    widget.load(filename);
    widget.show();
    widget.resize(500, 500);

    MyWidget my(filename);
    my.show();
    my.resize(500, 500);

    return app.exec();
}
