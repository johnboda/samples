#include <QtWidgets>
#include <QtGui/private/qguiapplication_p.h>
#include <QtGui/qpa/qplatformtheme.h>

int main(int a, char **b)
{
    QApplication app(a, b);

    qDebug() <<  QGuiApplicationPrivate::platform_theme;

    return app.exec();
}
