#include <QtWidgets>
#include <QDebug>

class TextItem : public QGraphicsTextItem
{
public:

    explicit TextItem(QGraphicsItem *parent = nullptr) : QGraphicsTextItem(parent) {}

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = nullptr) override
    {
        painter->setPen(Qt::red);
        painter->drawRect(boundingRect());

        QGraphicsTextItem::paint(painter, option, widget);
    }

    QRectF boundingRect() const override
    {
        auto r = QGraphicsTextItem::boundingRect();
        r.adjust(-10, 0, 0, 0);
        return r;
    }
};

class Item : public QGraphicsItem
{
public:

    explicit Item(QGraphicsItem *parent = nullptr) : QGraphicsItem(parent) {}

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = nullptr) override
    {
        painter->setPen(Qt::blue);
        painter->drawRect(boundingRect());
    }

    QRectF boundingRect() const override
    {
        return QRect(0, 0, 40, 40);
    }
};



class GraphicsScene : public QGraphicsScene
{
public:
    explicit GraphicsScene(QObject *parent = nullptr) : QGraphicsScene(parent)
    {
    }
};

class GraphicsView : public QGraphicsView
{
public:
    explicit GraphicsView(QWidget *parent = nullptr) : QGraphicsView(parent)
    {
    }

    void closeEvent(QCloseEvent*) override
    {
        qApp->quit();
    }
};

int main (int a, char **b)
{
    QApplication app(a, b);
    GraphicsView view;
    GraphicsScene scene;
    view.setScene(&scene);
    view.resize(600, 600);
    view.show();

    auto item = new TextItem();
    item->setPlainText("Test!");
    scene.addItem(item);

    auto item2 = new Item(item);

    QPushButton but("Dump Debug");
    but.connect(&but, &QPushButton::clicked, [&scene, &view, item, item2] {
        qDebug() << "Item rect: " << item->boundingRect() << "; pos=" << item->pos();
        qDebug() << "sub-item rect: " << item2->boundingRect() << "; pos=" << item2->pos();
        qDebug() << "Scene rect: " << scene.sceneRect();
        qDebug() << "View scene rect: " << view.sceneRect();
    });
    but.show();

    return app.exec();
}
