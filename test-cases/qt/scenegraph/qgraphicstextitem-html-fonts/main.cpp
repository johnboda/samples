#include <QtWidgets>
#include <QDebug>
#include <QTextBlock>

void iterateDocument(QTextDocument *doc)
{
    QTextBlock block = doc->begin();
    while (block.isValid())
    {
        qDebug() << block.charFormat().font().hintingPreference() << block.charFormat().fontHintingPreference()
                 << block.charFormat().font().styleStrategy() << block.charFormat().fontStyleStrategy();
        block = block.next();
    }



}

class GraphicsScene : public QGraphicsScene
{
public:
    explicit GraphicsScene(QObject *parent = nullptr) : QGraphicsScene(parent)
    {
        if (true)
        {
            auto item = addText("Test");
            QFont f("Noto Serif");
            f.setHintingPreference(QFont::PreferNoHinting);
            f.setStyleStrategy(QFont::NoAntialias);
            item->document()->setDefaultFont(f);
            item->setHtml(R"(<font size="6" face="Noto Serif" color="green">This is some text!</font>)");
            qDebug() << "Item font" << item->font();
            iterateDocument(item->document());
        }

        {
            auto item = addText("This is some text!");
            item->moveBy(0, 50);
            QFont f = item->font();
            f.setFamily("Noto Serif");
            f.setStyleStrategy(QFont::NoAntialias);
            f.setHintingPreference(QFont::PreferNoHinting);
            f.setPointSize(20);
            // item->setFont(f);
            item->document()->setDefaultFont(f);
            qDebug() << "Item font" << item->font();
            iterateDocument(item->document());
        }
    }
};

class GraphicsView : public QGraphicsView
{
public:
    explicit GraphicsView(QWidget *parent = nullptr) : QGraphicsView(parent)
    {
    }
};


int main (int a, char **b)
{
    QApplication app(a, b);
    GraphicsView view;
    GraphicsScene scene;
    view.setScene(&scene);
    view.resize(600, 600);
    view.show();

    QPushButton but("Dump Debug");
    but.connect(&but, &QPushButton::clicked, [&scene, &view] {
        qDebug() << "Scene rect: " << scene.sceneRect();
        qDebug() << "View scene rect: " << view.sceneRect();
    });
    but.show();

    return app.exec();
}
