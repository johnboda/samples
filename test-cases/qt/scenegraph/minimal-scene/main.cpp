#include <QtWidgets>
#include <QDebug>

class TextItem : public QGraphicsTextItem
{
public:

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = nullptr) override
    {
        painter->setPen(Qt::red);
        painter->drawRect(boundingRect());

        QGraphicsTextItem::paint(painter, option, widget);
    }

    QRectF boundingRect() const override
    {
        return QGraphicsTextItem::boundingRect();
    }
};

class GraphicsScene : public QGraphicsScene
{
public:
    explicit GraphicsScene(QObject *parent = nullptr) : QGraphicsScene(parent)
    {
        auto item = new TextItem();
        item->setPlainText("Test!");
        addItem(item);
    }
};

class GraphicsView : public QGraphicsView
{
public:
    explicit GraphicsView(QWidget *parent = nullptr) : QGraphicsView(parent)
    {
    }

    void closeEvent(QCloseEvent*) override
    {
        qApp->quit();
    }
};


int main (int a, char **b)
{
    const bool ishdpi = qgetenv("FOO")== QString("1");
    if (ishdpi) {
        QApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    }

    QApplication app(a, b);
    GraphicsView view;
    GraphicsScene scene;
    view.setScene(&scene);
    view.resize(600, 600);
    view.show();

    QPushButton but("Dump Debug");
    but.connect(&but, &QPushButton::clicked, [&scene, &view] {
        qDebug() << "Scene rect: " << scene.sceneRect();
        qDebug() << "View scene rect: " << view.sceneRect();
    });
    but.show();

    return app.exec();
}
