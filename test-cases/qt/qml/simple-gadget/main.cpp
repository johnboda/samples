#include "controller.h"

#include <QGuiApplication>
#include <QQuickView>
#include <QQmlContext>

int main(int a, char **b)
{
    QGuiApplication app(a, b);
    QQuickView view;
    view.setResizeMode(QQuickView::SizeRootObjectToView);
    Controller c;
    view.rootContext()->setContextProperty("_controller", &c);
    view.setSource(QUrl("qrc:/main.qml"));
    view.show();
    view.resize(500, 500);

    return app.exec();
}
