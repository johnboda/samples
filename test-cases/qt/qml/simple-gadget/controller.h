#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <QObject>
#include <QDebug>
#include <QVector>

class MyGadget2 {
    Q_GADGET
    Q_PROPERTY(int v READ v CONSTANT)
public:
    int v() const { return 1500; }
};
//Q_DECLARE_METATYPE(MyGadget2)

class MyGadget
{
    Q_GADGET
    Q_PROPERTY(int v READ v CONSTANT)
    //Q_PROPERTY(MyGadget2 gadget2 READ gadget2 CONSTANT)
    Q_PROPERTY(int writableInt READ writableInt WRITE setWritableInt)
public:
    int v() const { return m_v; }
    int m_v = 1;
    int m_writableInt = 42;

    operator bool() const {
        return false;
    }
    MyGadget2 gadget2() const { return MyGadget2(); }

    int writableInt() const { return m_writableInt; }
    void setWritableInt(int v) {
        qDebug() << "setWritableInt v=" << v;
        m_writableInt = v;
    }


};
Q_DECLARE_METATYPE(MyGadget)

class Controller : public QObject
{
    Q_OBJECT
    Q_PROPERTY(MyGadget mygadget READ mygadget WRITE setMyGadget NOTIFY fooChanged)
    Q_PROPERTY(QVector<MyGadget> mygadgets READ mygadgets NOTIFY foosChanged)
public:
    explicit Controller(QObject *parent = nullptr);
    MyGadget mygadget() const { return MyGadget(); }
    QVector<MyGadget> mygadgets() const { return m_gadgets; }

    void setMyGadget(MyGadget m) { qDebug() << "FOO!"; m_gadget = m; fooChanged(); };

    Q_INVOKABLE MyGadget getGadget() const { return m_gadget; }

    Q_INVOKABLE QVector<MyGadget> getGadgets() const { return m_gadgets; }

signals:
    void fooChanged();
    void foosChanged();

private:
    MyGadget m_gadget;
    QVector<MyGadget> m_gadgets;
};

#endif
