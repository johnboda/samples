import QtQuick 2.5

Item {
    width: 500
    height: 500
    property list<QtObject> mymodel: [
        Cell {
            foo: 1
            bar: 2
        },
        Cell {
            foo: 3
            bar: 4
        }
    ]

    Column {
        anchors.fill: parent
        Repeater {
            model:  mymodel
            Text {
                text: foo + ": " + bar + "; " + model["bar"]
            }
        }
    }
}
