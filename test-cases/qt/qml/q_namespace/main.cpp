#include "test.h"
#include <QApplication>
#include <QQmlEngine>
#include <QQuickView>

int main(int a, char**b)
{
    QApplication app(a, b);

    qmlRegisterUncreatableMetaObject(TestNS::staticMetaObject, // static meta object
                                    "com.kdab.namespace",          // import statement
                                    1, 0,                         // major and minor version of the import
                                    "TestNS",                 // name in QML
                                    "Error: only enums");

    QQuickView view;
    view.setSource(QUrl("main.qml"));
    view.show();

    return app.exec();
}
