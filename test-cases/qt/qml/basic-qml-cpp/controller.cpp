#include "controller.h"

#include <QQmlEngine>


using namespace NS;

Controller::Controller(QQuickView *view, QObject *parent)
    : QObject(parent)
    , m_someObj(new QObject(this))
    , m_view(view)
{
    qmlRegisterUncreatableType<Controller>("com.test", 1, 0, "Controller", "For enums access");
    qmlRegisterUncreatableType<MyGadget>("com.test", 1, 0, "MyGadget", "For enums access");

      qmlRegisterUncreatableMetaObject(NSBar::staticMetaObject, // static meta object
                                   "com.kdab.namespace",          // import statement
                                    1, 0,                         // major and minor version of the import
                                    "NSBar",                 // name in QML
                                    "Error: only enums");


    auto timer = new QTimer();
    connect(timer, &QTimer::timeout, [this] {
        enumChanged(EnumValue2);
    });
    timer->start(1000);
}

#include "name.moc"
