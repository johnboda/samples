#ifndef CONTROLLER_H
#define CONTROLLER_H

#include "namespacefoo.h"

#include <QObject>
#include <QTimer>
#include <QDebug>
#include <QQuickView>
#include <QtQml/private/qqmlengine_p.h>

extern "C"  char *qt_v4StackTrace(void *executionContext);

static QString jsStack(QV4::ExecutionEngine *engine) {
    QString stack;
    QVector<QV4::StackFrame> stackTrace = engine->stackTrace(10);
    for (int i = 0; i < stackTrace.count(); i++) {
        const QV4::StackFrame &frame = stackTrace.at(i);
        QString stackFrame;
        if (frame.column >= 0)
            stackFrame = QStringLiteral("%1 (%2:%3:%4)").arg(frame.function,
                                                             frame.source,
                                                             QString::number(frame.line),
                                                             QString::number(frame.column));
        else
            stackFrame = QStringLiteral("%1 (%2:%3)").arg(frame.function,
                                                             frame.source,
                                                             QString::number(frame.line));
        if (i)
            stack += QLatin1Char('\n');
        stack += stackFrame;
    }
    return stack;
}


namespace NS {
    class Foo {};
}

namespace NS {

class MyObj : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int prop READ prop CONSTANT)
public:
    enum MyObjEnum {
        EnumValue11 = 15,
        EnumValue22 = 16
    };
    Q_ENUM(MyObjEnum)
    int prop() const {
        return 1;
    }
};

class MyGadget {
    Q_GADGET
    Q_PROPERTY(MyGadagetEnum myGadgetEnum READ myGadgetEnum WRITE setMyGadgetEnum)
public:
    enum MyGadagetEnum {
        EnumValue1 = 15,
        EnumValue2 = 16
    };
    Q_ENUM(MyGadagetEnum)

    Q_INVOKABLE void test()
    {
        qDebug() << "Gadget invokable called";
    }

    void setMyGadgetEnum(MyGadagetEnum v)
    {
        m_v = v;
        qDebug() << "Gadget's setMyGadgetEnum called with v=" << v;
    }

    MyGadagetEnum myGadgetEnum() const
    {
        return EnumValue2;
    }

    MyGadagetEnum m_v;
};


class Controller : public QObject
{
    Q_OBJECT
    Q_PROPERTY(MyEnum myEnum READ myEnum WRITE setMyEnum NOTIFY myEnumChanged)
    Q_PROPERTY(NS::MyGadget myGadget READ myGadget WRITE setMyGadget NOTIFY myGadgetChanged)
    Q_PROPERTY(MyObj* myObject READ myObject CONSTANT)
    Q_PROPERTY(QObject *someObj READ someObj CONSTANT)
    Q_PROPERTY(NS::MyObj::MyObjEnum otherEnum READ otherEnum CONSTANT)
    Q_PROPERTY(NSBar::enumBar otherEnum2 READ otherEnum2 CONSTANT)
public:

    enum MyEnum {
        EnumValue1 = 12,
        EnumValue2 = 13
    };
    Q_ENUM(MyEnum)

    MyObj::MyObjEnum otherEnum() const { return MyObj::EnumValue11; }
    NSBar::enumBar otherEnum2() const { return NSBar::enumeratorBar; }

    explicit Controller(QQuickView *view, QObject *parent = nullptr);
    MyEnum myEnum() const { return EnumValue2; }

public slots:
    void slot1(MyEnum v)
    {
        qDebug() << "slot1" << v;
    }

    void setMyEnum(MyEnum v) {
        m_v = v;
        emit myEnumChanged(v);
    }

    void setMyGadget(MyGadget g)
    {
        m_gadget = g;
        emit myGadgetChanged(g);
    }

    MyGadget myGadget() const
    {
        return m_gadget;
    }

    MyObj *myObject() const
    {
        return new MyObj();
    }

    Q_INVOKABLE void printTrace() const
    {
        QQmlEnginePrivate *engineP = QQmlEnginePrivate::get(m_view->engine());
        QV4::ExecutionEngine *v4engine = engineP->v4engine();
        qDebug() << "Stack trace: for engine" << v4engine;
        //qDebug() << jsStack(v4engine);
        qt_v4StackTrace(((QV4::ExecutionEngine *)0x5555555d4e90)->currentContext());
        qDebug() << qt_v4StackTrace(v4engine->rootContext());
    }

    QObject *someObj() const { return m_someObj; }
    void deleteSomeObj() { delete m_someObj; }
    void crash() { deleteSomeObj(); deleteSomeObj(); }

signals:
    void enumChanged(NS::Controller::MyEnum v);
    void myEnumChanged(NS::Controller::MyEnum v);
    void myGadgetChanged(NS::MyGadget);
private:
    MyEnum m_v;
    MyGadget m_gadget;
    QObject *const m_someObj;
    QQuickView *const m_view;
};

}
#endif
