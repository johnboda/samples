import QtQuick 2.4
import com.test 1.0
import com.kdab.namespace 1.0

Rectangle {
    id: root
    color: "yellow"
    Text {
        text: "foo: " + _controller.otherEnum2
    }

    Connections {
        target: _controller
        onEnumChanged: {
            console.log("Signal received: v=" + v)
            //_controller.slot1(Controller.EnumValue2);
        }

        onMyEnumChanged: {
            console.log("Signal received1: v=" + v)
        }
    }

    MouseArea {
        anchors.fill: parent
        onClicked: {
            _controller.myEnum = Controller.EnumValue1
        }
    }


    Rectangle {
        color: "red"
        width: 150
        height: 50
        y: 300
        Text {
            text: "Provoke crash";
        }
        MouseArea {
            anchors.fill: parent
            onClicked: {
            }
        }
    }

}
