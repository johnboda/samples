#include "controller.h"

#include <QGuiApplication>
#include <QQuickView>
#include <QQmlContext>
#include <QQmlEngine>
#include <QtQml/private/qqmlengine_p.h>

using namespace NS;
extern "C"  char *qt_v4StackTrace(void *executionContext);

void printStackTrace()
{

}

int main(int a, char **b)
{
    QGuiApplication app(a, b);
    QQuickView view;
    view.setResizeMode(QQuickView::SizeRootObjectToView);
    Controller c(&view);
    view.rootContext()->setContextProperty("_controller", &c);
    MyGadget gadget1;
    view.rootContext()->setContextProperty("_gadget1", QVariant::fromValue<MyGadget>(gadget1));
    view.setSource(QUrl("qrc:/main.qml"));
    view.show();
    view.resize(1000, 500);

    QQmlEnginePrivate *engineP = QQmlEnginePrivate::get(view.engine());
    QV4::ExecutionEngine *v4engine = engineP->v4engine();



    //qDebug() << qt_v4StackTrace(v4engine);

    return app.exec();
}
