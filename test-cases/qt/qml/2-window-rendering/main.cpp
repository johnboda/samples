#include "controller.h"

#include <QGuiApplication>
#include <QQuickView>
#include <QQmlContext>

class MyWindow : public QQuickView
{
public:
    MyWindow()
        : QQuickView()
        , m_controller(new Controller())
    {
        setResizeMode(QQuickView::SizeRootObjectToView);
        rootContext()->setContextProperty("_controller", m_controller);
        setSource(QUrl("qrc:/main.qml"));


        connect(this, &QQuickWindow::beforeSynchronizing, [this] {
            qDebug() << objectName() << "QQuickWindow::beforeSynchronizing";
        });
        connect(this, &QQuickWindow::afterSynchronizing, [this] {
            qDebug() << objectName() << "QQuickWindow::afterSynchronizing";
        });
        connect(this, &QQuickWindow::beforeRendering, [this] {
            qDebug() << objectName() <<"QQuickWindow::beforeRendering";
        });
        connect(this, &QQuickWindow::afterRendering, [this] {
            qDebug() << objectName() << "QQuickWindow::afterRendering";
        });
        resize(500, 500);
        show();
    }

    Controller *const m_controller;
};

int main(int a, char **b)
{
    QGuiApplication app(a, b);
    MyWindow view;
    view.setObjectName("window 1");

    MyWindow view2;
    view2.setObjectName("window 2");

    return app.exec();
}
