import QtQuick 2.4

Rectangle {
    id: root
    color: "yellow"

    MouseArea {
        anchors.fill: parent
        onClicked:{
            anim.start();
        }
    }

    Rectangle {
        id: rect
        width: 50
        height: 50
        color: "red"

        PropertyAnimation {
            id: anim
            from: 0
            to: root.height - rect.height
            duration: 1000
            target: rect
            property: "y"
            loops: Animation.Infinite
        }
    }
}
