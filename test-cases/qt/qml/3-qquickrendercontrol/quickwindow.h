#ifndef QUICKWINDOW_H
#define QUICKWINDOW_H

#include <QQuickWindow>
#include <QQuickRenderControl>
#include <QTimer>
#include <QTime>

class RenderControl : public QQuickRenderControl
{
public:

    RenderControl()
    {
    }

    QWindow *renderWindow(QPoint *offset) override;

    QWindow *m_window = nullptr;
};

class QuickWindow : public QQuickWindow
{
    Q_OBJECT
public:
    explicit QuickWindow(RenderControl *c, const QUrl &sourceUrl);
    explicit QuickWindow(const QUrl &sourceUrl);
    void loadQml(const QUrl &sourceUrl);
    virtual void render();

    void exposeEvent(QExposeEvent *) override;

    void init();
    void requestUpdate();

private:
    QTime m_fpsCounter;
    QTimer m_renderTimer;
    RenderControl *m_renderControl = nullptr;
    QOpenGLContext *m_context = nullptr;
    bool m_quickInitialized = false;
    bool m_doSync = false;
    int m_numFrames = 0;
};

#endif
