######################################################################
# Automatically generated by qmake (3.0) Thu Oct 13 23:20:27 2016
######################################################################

TEMPLATE = app
TARGET = rendercontrol
INCLUDEPATH += .

# Input
HEADERS += controller.h \
    quickwindow.h
SOURCES += controller.cpp main.cpp \
    quickwindow.cpp
QT += quick
RESOURCES += resources.qrc
