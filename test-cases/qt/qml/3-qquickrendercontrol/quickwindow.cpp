#include "quickwindow.h"

#include <QQuickRenderControl>
#include <QQmlEngine>
#include <QQmlComponent>
#include <QOpenGLContext>
#include <QQuickItem>
#include <QDebug>
#include <QThread>

QWindow *RenderControl::renderWindow(QPoint *offset)
{
    if (offset)
        *offset = QPoint(0, 0);

    return m_window;
}

QuickWindow::QuickWindow(RenderControl *c, const QUrl &sourceUrl)
    : QQuickWindow(c)
    , m_renderControl(c)
    , m_context(new QOpenGLContext())
{
    c->m_window = this;
    loadQml(sourceUrl);

    QSurfaceFormat format;
    // Qt Quick may need a depth and stencil buffer. Always make sure these are available.
    format.setDepthBufferSize(16);
    format.setStencilBufferSize(8);
    setFormat(format);

    m_context->setFormat(format);
    m_context->create();

    m_renderTimer.setSingleShot(true);
    //m_updateTimer.setInterval(5);
    connect(&m_renderTimer, &QTimer::timeout, this, &QuickWindow::render);

    connect(m_renderControl, &QQuickRenderControl::renderRequested, this, &QuickWindow::requestUpdate);
    connect(m_renderControl, &QQuickRenderControl::sceneChanged, this, [this] {
        m_doSync = true;
        requestUpdate();
    });
}

QuickWindow::QuickWindow(const QUrl &sourceUrl)
    : QQuickWindow()
{
    loadQml(sourceUrl);
}

void QuickWindow::loadQml(const QUrl &sourceUrl)
{
    auto engine = new QQmlEngine(this);
    auto component = new QQmlComponent(engine, sourceUrl);
    auto item = qobject_cast<QQuickItem*>(component->create());
    item->setParentItem(contentItem());
    qDebug() << "Item=" << item;
}

void QuickWindow::render()
{
    if (m_numFrames == 0) {
        m_fpsCounter.start();
    } else {
        qDebug() << "QuickWindow::render() FPS =" << (m_numFrames / (m_fpsCounter.elapsed() / 1000.0));
        if ((m_numFrames % 200) == 0) {
            m_numFrames = 0;
            m_fpsCounter.start();
        }
    }

    m_context->makeCurrent(this);
    QTime profileTimer;
    profileTimer.start();

    const bool doSync = m_doSync;
    if (doSync) {
        m_renderControl->polishItems();
        m_renderControl->sync();
        m_doSync = false;
    }

    const int syncAndPolishTook = profileTimer.elapsed(); profileTimer.start();
    m_renderControl->render();
    const int renderTook = profileTimer.elapsed(); profileTimer.start();

    m_context->swapBuffers(this);

    const int swapTook = profileTimer.elapsed();
    const int totalTime = syncAndPolishTook + renderTook + swapTook;

    if (doSync) {
        qDebug() << "QuickWindow::render() ALL=" << totalTime << "; polish+sync="
                 << syncAndPolishTook << "; render=" << renderTook << "; swap=" << swapTook;
    } else {
        qDebug() << "QuickWindow::render() ALL=" << totalTime << "; render=" << renderTook << "; swap=" << swapTook;
    }

    m_numFrames++;
}

void QuickWindow::exposeEvent(QExposeEvent *e)
{
    if (isExposed() && !m_quickInitialized && m_renderControl) {
        init();
    }

    QWindow::exposeEvent(e);
}

void QuickWindow::init()
{
    qDebug() << "Init";
    m_context->makeCurrent(this);
    m_renderControl->initialize(m_context);
    m_quickInitialized = true;
}

void QuickWindow::requestUpdate()
{
    if (!m_renderTimer.isActive())
        m_renderTimer.start();
}
