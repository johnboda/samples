#include "controller.h"
#include "quickwindow.h"

#include <QGuiApplication>
#include <QQuickView>
#include <QTimer>
#include <QElapsedTimer>

#define NO_RENDER_CONTROL 1

int main(int a, char **b)
{
    QGuiApplication app(a, b);

    const QUrl source = QUrl("qrc:/main.qml");

#if defined(NO_RENDER_CONTROL)
    QQuickView window;
    window.setSource(source);
#else
    RenderControl control;
    QuickWindow window(&control, source);
#endif

    QTimer t;
    t.setInterval(1);
    QElapsedTimer elap;
    elap.start();
    t.start();
    t.connect(&t, &QTimer::timeout, [&elap] {
        static int last = 0;
        int current = elap.elapsed();
        qDebug() << "Elapsed=" << (current - last);
        last = elap.elapsed();
    });

    window.show();
    window.resize(500, 500);
    return app.exec();
}
