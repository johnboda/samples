import QtQuick 2.4

Rectangle {
    id: root
    color: "red"
    anchors.fill: parent
    MouseArea {
        anchors.fill: parent
        onClicked: {
            anim.start();
        }
    }

    Repeater {
        model:2 // 30000
        Rectangle {
            anchors.right: parent.right
            color: "blue"
            width: 50
            height: 50
            y: rect.y
        }
    }

    Rectangle {
        id: rect
        color: "yellow"
        height: 50
        width: 50
        PropertyAnimation {
            id: anim
            target: rect
            property: "y"
            from: 0
            duration: 1000
            to: root.height - rect.height
            loops: PropertyAnimation.Infinite
        }
    }
}
