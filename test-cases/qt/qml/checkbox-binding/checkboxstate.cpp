#include "checkboxstate.h"

CheckBoxState::CheckBoxState(QObject *parent)
    : QObject(parent)
{
}

bool CheckBoxState::checked() const
{
    return m_checked;
}

void CheckBoxState::setChecked(bool checked)
{
    if (m_checked == checked)
        return;

    m_checked = checked;
    emit checkedChanged(m_checked);
}
