import QtQuick 2.10
import Test 1.0

Item {
    id: root

    property alias checked: __internal.checked
    property alias text: label.text

    implicitHeight: checkbox.height
    implicitWidth: checkbox.width + label.implicitWidth + 4

    Text {
        id: label
    }

    Rectangle {
        id: checkbox
        implicitWidth: 40
        implicitHeight: 40

        anchors {
            left: label.right
        }

        CheckBoxState {
            // Instantiates the C++ object to keep state
            id: __internal
        }

        Rectangle {
            color: checked ? 'black' : 'white'
            anchors {
                fill: parent
                margins: 8
            }
        }

        border {
            color: "black"
            width: 1
        }

        MouseArea {
            anchors.fill: parent
            onClicked: {
                // Calling the actual setter slot/invokable instead of doing "__internal.checked = ", so binding doesn't break
                __internal.setChecked(!__internal.checked);
            }
        }
    }
}


