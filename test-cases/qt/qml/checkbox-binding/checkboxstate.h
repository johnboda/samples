#ifndef CHECKBOXSTATE_H
#define CHECKBOXSTATE_H

#include <QObject>

class CheckBoxState : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool checked READ checked WRITE setChecked NOTIFY checkedChanged)

public:
    explicit CheckBoxState(QObject *parent = nullptr);
    bool checked() const;
signals:
    void checkedChanged(bool checked);
public slots:
    void setChecked(bool checked); // Be sure to make it a slot
private:
    bool m_checked = false;
};

#endif
