#ifndef SOMEOTHERCONTROLLER_H
#define SOMEOTHERCONTROLLER_H

#include <QObject>
#include <QColor>

class SomeOtherController : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool isYellow READ isYellow WRITE setIsYellow NOTIFY isYellowChanged)
public:
    explicit SomeOtherController(QObject *parent = nullptr);
    bool isYellow() const;


signals:
    void isYellowChanged(bool is);

public slots:
    void setIsYellow(bool is); // Be sure to make it a slot

private:
    bool m_isYellow = false;
};

#endif
