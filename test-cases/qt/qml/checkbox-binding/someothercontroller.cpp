#include "someothercontroller.h"

SomeOtherController::SomeOtherController(QObject *parent) : QObject(parent)
{

}

bool SomeOtherController::isYellow() const
{
    return m_isYellow;
}

void SomeOtherController::setIsYellow(bool is)
{
    if (is != m_isYellow) {
        m_isYellow = is;
        emit isYellowChanged(is);
    }
}

