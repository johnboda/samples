import QtQuick 2.4
import Test 1.0
Rectangle {
    id: root
    color: colorController.isYellow ? "yellow" : "red"

    CheckBox {
        id: check1
        text: "Make it Yellow"
        checked: colorController.isYellow
    }

    SomeOtherController {
        id: colorController
        isYellow: check1.checked
    }


    Rectangle {
        id: button
        color: "gray"
        height: 50
        width: 200
        Text {
            text: "Make it RED"
        }
        MouseArea {
            anchors.fill: parent
            // Calling the actual setter slot/invokable instead of doing "isYellow = false", so binding doesn't break
            onClicked: colorController.setIsYellow(false)
        }
        anchors {
            right: parent.right
            bottom: parent.bottom
        }
        border {
            color: "black"
            width: 1
        }
    }
}
