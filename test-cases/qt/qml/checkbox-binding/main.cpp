#include "checkboxstate.h"
#include "someothercontroller.h"
#include <QGuiApplication>
#include <QQuickView>
#include <QQmlContext>

int main(int a, char **b)
{
    QGuiApplication app(a, b);
    QQuickView view;
    view.setResizeMode(QQuickView::SizeRootObjectToView);

    qmlRegisterType<CheckBoxState>("Test", 1, 0, "CheckBoxState");
    qmlRegisterType<SomeOtherController>("Test", 1, 0, "SomeOtherController");

    view.setSource(QUrl("qrc:/main.qml"));
    view.show();
    view.resize(500, 500);

    return app.exec();
}
