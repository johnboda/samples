#include <QtWidgets>
#include <QDebug>
#include <QAction>
#include <QtCore/private/qobject_p.h>

class MyMainWindow : public QMainWindow
{
public:
    explicit MyMainWindow(QWidget *parent = nullptr)
        : QMainWindow(parent)
    {
        setObjectName("MyMainWindow");
    }

    bool event(QEvent *e) override
    {
        if (e->type() == QEvent::Shortcut) {
            qDebug() << "Got Shortcut event" << objectName();
        } else if (e->type() == QEvent::ShortcutOverride) {
            qDebug()<< "Got ShortcutOverride event" << objectName() ;

            if (m_override)
                e->accept();
            else
                e->ignore();
            return true;
        }

        return QMainWindow::event(e);
    }
    bool m_override = false;
};

class MyWidget : public QWidget
{
public:
    explicit MyWidget(bool installFilter = false, QWidget *parent = nullptr)
        : QWidget(parent)
    {
        if (installFilter)
            qApp->installEventFilter(this);        
    }

    bool event(QEvent *e) override
    {
        if (e->type() == QEvent::Shortcut) {
            qDebug() << "Got Shortcut event" << objectName();
        } else if (e->type() == QEvent::ShortcutOverride) {
            qDebug()<< "Got ShortcutOverride event" << objectName() ;

            if (m_overridde)
                e->accept();
            else
                e->ignore();
            return true;
        }

        return QWidget::event(e);
    }

    bool eventFilter(QObject *obj, QEvent *e) override
    {
        if (e->type() == QEvent::Shortcut || e->type() == QEvent::ShortcutOverride || e->type() == QEvent::KeyPress ||  e->type() == QEvent::KeyRelease) {
            if (!qobject_cast<QWindow*>(obj) && m_qappDebug)
                qDebug() << "qApp:" << "Sending event " << e->type() << "to" << obj;
        } else if (e->type() == QEvent::MetaCall) {
            auto mc = static_cast<QMetaCallEvent*>(e);
          //  qDebug() << "Metacall" << mc->sender() << ": receiver=" << obj;
        }
       // qDebug() << e << obj;
        return false;
    }

    void resizeEvent(QResizeEvent *e) override { return QWidget::resizeEvent(e); }
    void paintEvent(QPaintEvent *e) override { return QWidget::paintEvent(e); }
    void mouseMoveEvent(QMouseEvent *e) override { return QWidget::mouseMoveEvent(e); }
    void keyPressEvent(QKeyEvent *e) override
    {
        qDebug() << "QWidget::keyPressEvent" << objectName();
        return QWidget::keyPressEvent(e);
    }

    bool m_overridde = false;
    bool m_qappDebug = false;
};

int main (int a, char **b)
{
    QApplication app(a, b);

    qDebug() << "QApplication::instance()->testAttribute(Qt::AA_DontShowShortcutsInContextMenus)"
             << QApplication::instance()->testAttribute(Qt::AA_DontShowShortcutsInContextMenus);

    MyMainWindow mainWindow;
    MyWidget w(true);
    mainWindow.setCentralWidget(&w);
    auto lay = new QVBoxLayout(&w);
    w.setObjectName("main");
    auto w2 = new MyWidget(); lay->addWidget(w2);
    w.setObjectName("widget2");

    auto act = new QAction(&w);
    act->setShortcut(QKeySequence::Delete);
    act->setShortcutContext(Qt::WindowShortcut);
    act->connect(act, &QAction::triggered, [] {
        qDebug() << "Action executed";
    });
    w.addAction(act);

    auto cb = new QCheckBox("override shortcut");
    cb->setChecked(false);
    cb->connect(cb, &QCheckBox::toggled, [&w, w2, &mainWindow] (bool checked) {
        w.m_overridde = checked;
        w2->m_overridde = checked;
        mainWindow.m_override = checked;
    });

    auto cb2 = new QCheckBox("qApp debug");
    cb2->setChecked(false);
    cb2->connect(cb2, &QCheckBox::toggled, [&w, w2] (bool checked) {
        w.m_qappDebug = checked;
        w2->m_qappDebug = checked;
    });

    auto createMenuButton = new QPushButton("Create Menu Bar with shortcut");
    QObject::connect(createMenuButton, &QPushButton::clicked, [createMenuButton, &mainWindow] {
        auto menu = new QMenu("File");
        auto a = new QAction("Delete", menu);
        a->setShortcut(QKeySequence::Delete);
        a->setShortcutContext(Qt::WidgetShortcut);
        QObject::connect(a, &QAction::triggered, [] {
            qDebug() << "Delete action triggered from menu";
            qDebug() << "Focus widget is " << qApp->focusObject();
        });
        menu->addAction(a);
        mainWindow.menuBar()->addMenu(menu);
        createMenuButton->setEnabled(false);
    });

    lay->addWidget(cb);
    lay->addWidget(cb2);
    lay->addWidget(createMenuButton);

    mainWindow.resize(600, 600);
    mainWindow.show();

    return app.exec();
}
