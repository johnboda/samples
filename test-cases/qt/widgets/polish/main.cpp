#include <QtWidgets>
#include <QDebug>

class PolishCompressor : public QObject
{
public:
    PolishCompressor() : QObject()
    {
        qApp->installEventFilter(this);
    }

    ~PolishCompressor()
    {
        qApp->removeEventFilter(this);
        for (QObject* o : m_queue)
        {
            if (auto widget = qobject_cast<QWidget*>(o))
            {
                if (widget->isVisible())
                {
                    qApp->postEvent(widget, new QEvent(QEvent::Polish));
                }
            }
        }
    }

protected:
    bool eventFilter(QObject* o, QEvent* e) override
    {
        if (e->type() == QEvent::Polish)
        {
            if (!m_queue.contains(o))
            {
                m_queue << QPointer<QObject>(o);
            }
            return true;
        }

        return false;
    }

private:
    Q_DISABLE_COPY(PolishCompressor)
    QVector<QPointer<QObject>> m_queue;
};

class MyWidget : public QWidget
{
public:
    explicit MyWidget(QWidget *parent = nullptr) : QWidget(parent)
    {
        qApp->installEventFilter(this);
    }

    bool event(QEvent *e) override { return QWidget::event(e); }
    void resizeEvent(QResizeEvent *e) override { return QWidget::resizeEvent(e); }
    void paintEvent(QPaintEvent *e) override { return QWidget::paintEvent(e); }
    void mouseMoveEvent(QMouseEvent *e) override { return QWidget::mouseMoveEvent(e); }
    bool eventFilter(QObject *, QEvent *)  override
    {
        return false;
    }
};

int main (int a, char **b)
{
    QApplication app(a, b);
    MyWidget w;
    w.setObjectName("MainWindow");
    w.resize(600, 600);
    w.show();

    PolishCompressor pc;
    auto lay = new QVBoxLayout(&w);
    auto le = new QLineEdit(&w);
    lay->addWidget(le);
    qApp->setStyleSheet("#MainWindow QLineEdit { background: yellow; max-width: 30;}");

    MyWidget w2;
    w2.show();


    QPushButton but("Dump Debug");
    but.connect(&but, &QPushButton::clicked, [&w, le, &w2] {
        qDebug() << "Repolish start";
        qApp->setStyleSheet("");
        qDebug() << le->maximumSize();


        //qApp->style()->unpolish(le);
        /*w.setObjectName("");
        qApp->postEvent(le, new QEvent(QEvent::Polish));
        qApp->style()->polish(le);
        qApp->postEvent(le, new QEvent(QEvent::LayoutRequest));
        qApp->postEvent(le->parentWidget(), new QEvent(QEvent::LayoutRequest));*/


        qDebug() << "Repolish end";
    });
    but.show();

    qApp->removeEventFilter(&rc);

    return app.exec();
}
