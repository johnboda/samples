// Tests if an invisible mouse grabber continues to receive mouse events

#include <QtWidgets>

class Obj : public QObject {
public:
  bool eventFilter(QObject *t, QEvent *ev) override {

      if (ev->type() == QEvent::MouseMove) {
        static int c = 0;
        c++;
        qDebug() << "MouseMove! " << c << t;
      }

      return false;
  }
};

int main(int a, char **b)
{
    QApplication app(a, b);
    app.installEventFilter(new Obj());

    auto window = new QWidget();
    auto l = new QVBoxLayout(window);
    auto but = new QPushButton("Hello");
    l->addWidget(but);
    but->connect(but, &QPushButton::pressed, but, [but] {
        qDebug() << "Pressed";
        but->grabMouse();
        but->hide();
    });


    window->show();


    return app.exec();
}
