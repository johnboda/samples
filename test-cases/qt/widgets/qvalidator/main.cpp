/**
 * A basic QMainWindow
 */
#include <QtWidgets>
#include <QDebug>

class TrueFalseValidator : public QValidator
{
    public:

    State validate(QString &input, int &) const override
    {
        if (input == QLatin1String("true") || input == QLatin1String("false"))
            return Acceptable;

        if (QStringLiteral("true").startsWith(input) || QStringLiteral("false").startsWith(input))
            return Intermediate;

        return Invalid;
    }
};

int main (int a, char **b)
{
    QApplication app(a, b);
    QLineEdit le;
    le.setValidator(new TrueFalseValidator());

    QObject::connect(&le, &QLineEdit::textChanged, [&le] {
        qDebug() << le.text() << " is acceptable?" << le.hasAcceptableInput();
    });

    le.show();
    return app.exec();
}
