#include <QtWidgets>
#include <QDebug>

class MyWidget : public QWidget
{
public:
    explicit MyWidget(QWidget *parent = nullptr) : QWidget(parent)
    {
        qApp->installEventFilter(this);

        auto l = new QVBoxLayout(this);
        l->addWidget(new QPushButton("test"));
        l->addWidget(new QPushButton("test"));
        l->addWidget(new QPushButton("test"));
        l->addWidget(new QPushButton("test"));


    }

    bool event(QEvent *e) override { return QWidget::event(e); }
    void resizeEvent(QResizeEvent *e) override { return QWidget::resizeEvent(e); }
    void paintEvent(QPaintEvent *e) override { return QWidget::paintEvent(e); }
    void mouseMoveEvent(QMouseEvent *e) override { return QWidget::mouseMoveEvent(e); }
    bool eventFilter(QObject *, QEvent *)  override
    {
        return false;
    }
};

int main (int a, char **b)
{
    QApplication app(a, b);
    QMainWindow w;
    w.setDockOptions(QMainWindow::GroupedDragging | QMainWindow::AllowTabbedDocks | QMainWindow::AllowNestedDocks);
    w.resize(600, 600);
    w.show();

    QDockWidget *dock1 = new QDockWidget("left", &w);
    dock1->setWidget(new MyWidget());

    dock1->resize(100, 100);

    QDockWidget *dock2 = new QDockWidget("left", &w );
    dock2->setWidget(new MyWidget());
    w.addDockWidget(Qt::RightDockWidgetArea, dock1);
    w.addDockWidget(Qt::LeftDockWidgetArea, dock2);
    w.setCentralWidget(new QPushButton("Central"));

    QPushButton but("Dump Debug");
    but.connect(&but, &QPushButton::clicked, [&w, dock1, dock2] {
        dock1->resize(400, dock1->height());
        dock2->widget()->resize(100, 100);
    });
    but.show();

    return app.exec();
}
