/**
 * A basic QMainWindow
 */
#include <QtWidgets>
#include <QDebug>

class CentralWidget : public QWidget
{
public:
    explicit CentralWidget(QWidget *parent = nullptr)
        : QWidget(parent)
    {
    }
};

class MainWindow : public QMainWindow
{
public:
    explicit MainWindow(QWidget *parent = nullptr)
        : QMainWindow(parent)
    {
        createMenus();
        createToolBar();
        setCentralWidget(new CentralWidget());
    }

    void createMenus()
    {
        auto fileMenu = menuBar()->addMenu(tr("&File"));
        fileMenu->addAction(new QAction("New", this));
        fileMenu->addAction(new QAction("Save", this));
        fileMenu->addAction(new QAction("Open", this));
    }

    void createToolBar()
    {
        QToolBar *tb = addToolBar("ToolBar");
        auto a = new QAction("Open", this);
        tb->addAction(a);
        tb->setEnabled(false);
        a->setShortcut(QKeySequence("Ctrl+O"));
        connect(a, &QAction::triggered, [a] {
            qDebug() << a->associatedWidgets();
        });
    }
};

bool operator<(const QSize s, const QSize)
{
    return true;

}

int main (int a, char **b)
{

    qDebug() << std::min(QSize(1, 1), QSize(2, 2));

    QApplication app(a, b);
    MainWindow w;
    w.resize(600, 600);
    w.show();
    return app.exec();
}
