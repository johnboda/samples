#include <QtWidgets>
#include <QDebug>

class MyWidget : public QWidget
{
public:
    explicit MyWidget(QWidget *parent = nullptr) : QWidget(parent)
    {
        setObjectName("MyWidget");
        qApp->installEventFilter(this);
    }

    bool event(QEvent *e) override {
        if (e->type() == QEvent::MouseMove)
            qDebug() << "Widget Mouse move";
        return QWidget::event(e);
    }
    void resizeEvent(QResizeEvent *e) override { return QWidget::resizeEvent(e); }
    void paintEvent(QPaintEvent *e) override { return QWidget::paintEvent(e); }
    void mouseMoveEvent(QMouseEvent *e) override
    {
        return QWidget::mouseMoveEvent(e);
    }

    bool eventFilter(QObject *o, QEvent *e)
    {
        if (e->type() == QEvent::MouseMove)
            qDebug() << "Mouse move to " << o;

        return false;
    }
};

int main (int a, char **b)
{
    QApplication app(a, b);
    MyWidget w;
    w.resize(600, 600);
    w.show();

    QPushButton but("Dump Debug");
    but.connect(&but, &QPushButton::clicked, [&w] {

    });
    but.show();

    return app.exec();
}
