#include <QtWidgets> 

class MyWidget : public QWidget
{
    public:
    MyWidget() {
        create();
        qDebug() << "Font" << font();
        qDebug() << "Default font.pointSize" << font().pointSizeF();
        qDebug() << "Default font.pixelSize" << font().pixelSize();
        qDebug() << "lDPI" << this->windowHandle()->screen()->logicalDotsPerInch();
        qDebug() << "pDPI" << this->windowHandle()->screen()->physicalDotsPerInch();
    }
};

int main(int a, char **b)
{
    QApplication::setAttribute(Qt::AA_UseHighDpiPixmaps);
    // QApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QApplication app(a, b);

    MyWidget  widget;

    return app.exec();
}