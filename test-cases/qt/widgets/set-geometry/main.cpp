#include <QtWidgets>
#include <QDebug>

class MyWidget : public QWidget
{
public:
    explicit MyWidget(QWidget *parent = nullptr) : QWidget(parent)
    {
        qApp->installEventFilter(this);
    }

    bool event(QEvent *e) override { return QWidget::event(e); }
    void resizeEvent(QResizeEvent *e) override { return QWidget::resizeEvent(e); }
    void paintEvent(QPaintEvent *e) override { return QWidget::paintEvent(e); }
    void mouseMoveEvent(QMouseEvent *e) override { return QWidget::mouseMoveEvent(e); }
    bool eventFilter(QObject *, QEvent *)  override
    {
        return false;
    }
};

int main (int a, char **b)
{
    QApplication app(a, b);
    MyWidget w;
    w.setGeometry(0,20,500,500);
    qDebug() << w.geometry() << w.frameGeometry() << w.windowFlags();
    w.setWindowFlag(Qt::WindowSystemMenuHint, false);
    w.show();
    qDebug() << w.geometry() << w.frameGeometry();


    w.show();

    QPushButton but("Dump Debug");
    but.connect(&but, &QPushButton::clicked, [&w] {
        qDebug() << "w1" << w.geometry() << w.frameGeometry();
    });
    but.show();

    return app.exec();
}
