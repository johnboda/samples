#include <QtWidgets>
#include <QDebug>


int main (int a, char **b)
{
    QApplication app(a, b);
    QWidget w;

    auto layout = new QVBoxLayout(&w);
    auto spinbox = new QSpinBox();
    auto spinbox2 = new QDoubleSpinBox();
    auto le = new QLineEdit();
    layout->addWidget(spinbox);
    layout->addWidget(spinbox2);
    layout->addWidget(le);
    w.resize(600, 600);
    w.show();
    return app.exec();
}

