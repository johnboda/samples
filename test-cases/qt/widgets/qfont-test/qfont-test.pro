TEMPLATE = app
TARGET = qfont-test
INCLUDEPATH += .

# Input
SOURCES += main.cpp
QT += widgets

gcc: QMAKE_CXXFLAGS += "-Wno-expansion-to-defined"
CONFIG += console
QT += printsupport
