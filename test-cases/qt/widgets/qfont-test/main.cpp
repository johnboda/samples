/*
  Copyright (C) 2014 John Boda

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/



// Sample to test various QFont properties
// Uses FreeType by default. Comment line to change.

#include <QtWidgets>
#include <QFontDatabase>
#include <QDebug>
#include <QPrinter>

struct FontOpts
{
    bool kerningEnabled = true;
    bool subpixelAAEnabled = true;
    bool shapingEnabled = true;
    bool sizesInPixels = false;
    bool alsoApplyToMainWindow = true;
    bool aaEnabled = true;
    QFont::HintingPreference hintingPref = QFont::PreferFullHinting;
    int weight = 50; // normal
    void apply(QFont &f)
    {
        f.setKerning(kerningEnabled);
        int strategy = 0;
        if (!subpixelAAEnabled)
            strategy |= QFont::NoSubpixelAntialias;
#if QT_VERSION >= QT_VERSION_CHECK(5,10,0)
        if (!shapingEnabled)
            strategy |= QFont::PreferNoShaping;
#endif

        if (aaEnabled)
            strategy |= QFont::PreferAntialias;
        else
            strategy |= QFont::NoAntialias;

        f.setStyleStrategy((QFont::StyleStrategy) strategy);
        f.setHintingPreference(hintingPref);
        f.setWeight(weight);
    }
};

class MyWidget : public QWidget
{
public:
    explicit MyWidget(const QStringList &families, QWidget *parent = nullptr)
        : QWidget(parent)
        , fontFamilies(families)
    {
    }

    void paintEvent(QPaintEvent *) override
    {
        QPainter p(this);

        QPoint pt(50, 100);
        for (auto size : { pointSize, pointSize2, pointSize3 })
            for (auto &family : fontFamilies) {
                QFont f(family);
                fopts.apply(f);

                if (fopts.sizesInPixels)
                    f.setPixelSize(size);
                else f.setPointSize(size);

                p.setFont(f);
                p.drawText(pt, text + QString("    (%1 pt)").arg(size));
                pt.setY(pt.y() + 90);
            }

        pt.setY(pt.y() + 100);

        if (fopts.alsoApplyToMainWindow) {
            QFont f = qApp->font();
            fopts.apply(f);
            qApp->setFont(f);
        }
    }


    void setFontOpts(FontOpts opts)
    {
        fopts = opts;
        update();
    }

    int pointSize = 18;
    int pointSize2 = 22;
    int pointSize3 = 50;
    FontOpts fopts;
    QString text = "Sample text";

private:
    const QStringList fontFamilies;
};

int main(int a, char **b)
{
#ifdef Q_OS_WIN
    qputenv("QT_QPA_PLATFORM", "windows:fontengine=freetype");
    qputenv("QT_SUBPIXEL_AA_TYPE", "RGB");
#endif

    QApplication app(a, b);
    qDebug() << app.font();

    auto args = app.arguments();
    args.removeAt(0);
    QStringList fonts;
    for (auto arg :args) {
        if (QFile::exists(arg))
            fonts << arg;
    }

    QStringList families;
    if (fonts.isEmpty()) {
        qDebug() << "No font specified, using system font";
        families << qApp->font().family();
    }

    for (auto &f : fonts) {
        const int id = QFontDatabase::addApplicationFont(f);
        if (id == -1) {
            qDebug() << "Failed to load font " << f << "; Exists ? " << QFile::exists(f);
        } else {
            families << QFontDatabase::applicationFontFamilies(id);
            qDebug() << "Loaded these families:" << families;
        }
    }

    FontOpts fopts;

    QScrollArea scrollArea;
    auto w = new MyWidget(families);
    scrollArea.setWidget(w);
    w->resize(3000, 7000);
    scrollArea.resize(1300, 1000);
    scrollArea.show();

    QGroupBox control;
    auto lay = new QVBoxLayout(&control);

    auto check = new QCheckBox("Kerning");
    check->setChecked(fopts.kerningEnabled);
    lay->addWidget(check);
    QObject::connect(check, &QCheckBox::toggled, [w, &fopts] (bool checked) {
        fopts.kerningEnabled = checked;
        w->setFontOpts(fopts);
    });

    check = new QCheckBox("Shaping");
    check->setChecked(fopts.shapingEnabled);

#if QT_VERSION<QT_VERSION_CHECK(5,10,0)
    check->setEnabled(false);
#endif
    lay->addWidget(check);
    QObject::connect(check, &QCheckBox::toggled, [w, &fopts] (bool checked) {
        fopts.shapingEnabled = checked;
        w->setFontOpts(fopts);
    });

    check = new QCheckBox("subpixel");
    check->setChecked(fopts.subpixelAAEnabled);
    lay->addWidget(check);
    QObject::connect(check, &QCheckBox::toggled, check, [w, &fopts] (bool checked) {
        fopts.subpixelAAEnabled = checked;
        w->setFontOpts(fopts);
    });

    check = new QCheckBox("anti-aliasing");
    check->setChecked(fopts.aaEnabled);
    lay->addWidget(check);
    QObject::connect(check, &QCheckBox::toggled, check, [w, &fopts] (bool checked) {
        fopts.aaEnabled = checked;
        w->setFontOpts(fopts);
    });

    auto group = new QGroupBox();
    auto vbox = new QVBoxLayout(group);

    struct HintRadios {
        typedef QVector<HintRadios> List;
        QString title;
        QFont::HintingPreference hintingPreference;
    };
    HintRadios::List radios;
    radios.append({ "default hinting ", QFont::PreferDefaultHinting });
    radios.append({ "no hinting ", QFont::PreferNoHinting });
    radios.append({ "full hinting ", QFont::PreferFullHinting });
    radios.append({ "vertical hinting ", QFont::PreferVerticalHinting });

    for (auto radio : radios) {
        lay->addWidget(group);
        auto r = new QRadioButton(radio.title);
        r->setChecked(fopts.hintingPref == radio.hintingPreference);
        vbox->addWidget(r);
        auto pref = radio.hintingPreference;
        QObject::connect(r, &QRadioButton::toggled, [w, pref, &fopts] (bool checked) {
            if (checked) {
                fopts.hintingPref = pref;
                w->setFontOpts(fopts);
            }
        });
    }

    check = new QCheckBox("sizes in pixels");
    check->setChecked(fopts.sizesInPixels);
    lay->addWidget(check);
    QObject::connect(check, &QCheckBox::toggled, [w, &fopts] (bool checked) {
        fopts.sizesInPixels = checked;
        w->setFontOpts(fopts);
    });

    check = new QCheckBox("Also apply to MainWindow");
    check->setChecked(fopts.alsoApplyToMainWindow);
    lay->addWidget(check);
    QObject::connect(check, &QCheckBox::toggled, [w, &fopts] (bool checked) {
        fopts.alsoApplyToMainWindow = checked;
        w->setFontOpts(fopts);
    });

    auto le = new QLineEdit();
    lay->addWidget(le);
    le->setText(w->text);
    QObject::connect(le, &QLineEdit::textChanged, [w] (const QString &text) {
        w->text = text;
        w->update();
    });

    auto spin = new QSpinBox();
    spin->setValue(w->pointSize);
    lay->addWidget(spin);
    QObject::connect(spin, &QSpinBox::editingFinished, [w, spin] {
        w->pointSize = spin->value();
        w->update();
    });

    spin = new QSpinBox();
    spin->setValue(w->pointSize2);
    lay->addWidget(spin);
    QObject::connect(spin, &QSpinBox::editingFinished, [w, spin] {
        w->pointSize2 = spin->value();
        w->update();
    });

    spin = new QSpinBox();
    spin->setValue(w->pointSize3);
    lay->addWidget(spin);
    QObject::connect(spin, &QSpinBox::editingFinished, [w, spin] {
        w->pointSize3 = spin->value();
        w->update();
    });


    spin = new QSpinBox();
    spin->setValue(fopts.weight);
    QObject::connect(spin, static_cast<void(QSpinBox::*)(int)>(&QSpinBox::valueChanged), [w, spin, &fopts] {
        fopts.weight = spin->value();
        w->setFontOpts(fopts);
    });

    auto hl = new QHBoxLayout();
    hl->addWidget(new QLabel("weight"));
    hl->addWidget(spin);
    lay->addLayout(hl);

    auto cb = new QComboBox();
    cb->addItem("Some long text");
    lay->addWidget(cb);

    auto button = new QPushButton("Save to PDF via QPdfWriter");
    QObject::connect(button, &QPushButton::clicked, [w] {
        QPdfWriter writer("saved_pdfwriter.pdf");
        w->render(&writer);
    });
    lay->addWidget(button);

    button = new QPushButton("Save to PDF via QPrinter");
    QObject::connect(button, &QPushButton::clicked, [w] {
        QPrinter printer;
        printer.setOutputFormat(QPrinter::PdfFormat);
        printer.setOutputFileName("saved_qprinter.pdf");
        w->render(&printer);
    });
    lay->addWidget(button);

    lay->addStretch();
    control.resize(600,600);
    control.show();

    return app.exec();
}
