#include <QtWidgets>

static QString s_text = "0123456789";

int pointsToPixels(qreal points, int dpi)
{
    // Formula copied from qfontdatabase.cpp QFontDatabase::load()
    const qreal pixels = std::floor(((points * dpi) / 72) * 100 + 0.5) / 100;
    qDebug() << points << "points are" << qRound(pixels) << "pixels"
             << "; actually=" << pixels;
    return qRound(pixels);
}

qreal pixelToPoint(int pixels, int dpi)
{
    qreal points = pixels*72.0/dpi;
    qDebug() << pixels << "pixels are" << points << "points";
    return points;
}



class MyWidget : public QWidget
{
public:
    MyWidget() : QWidget() {
        resize(500, 500);

        pixelToPoint(19, logicalDpiY());
        pixelToPoint(20, logicalDpiY());

    }

    void paintEvent(QPaintEvent *) override
    {
        QPainter p(this);
        QRect box = QRect(100, 100, 210, 50);
        p.drawRect(box);

        QFont arial("Arial");
        const int pxSize = pointsToPixels(14, logicalDpiY());
        arial.setPixelSize(pxSize);
        p.setFont(arial);
        p.drawText(box, Qt::AlignVCenter, s_text);

        QRect box2 = QRect(100, 300, 210, 50);
        p.drawRect(box2);

        QFontMetrics fm(arial);
        qDebug() << "bounding box px=" << fm.boundingRect(s_text).size();

        QFont arial2("Arial");
        arial2.setPointSize(14);
        p.setFont(arial2);
        p.drawText(box2, Qt::AlignVCenter, s_text);
        QFontMetrics fm2(arial2);
        qDebug() << "bounding box pt=" << fm2.boundingRect(s_text).size();

    }
};

int main(int a, char **b)
{


    const bool ishdpi = qgetenv("FOO")== QString("1");
    if (ishdpi) {
        QApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    }

    QApplication app(a, b);

    auto screens = app.screens();
    QScreen *screen = screens.at(0);
    qDebug() << "Phys=" << screen->physicalDotsPerInch() << "; logical=" << screen->logicalDotsPerInch()
             << "; pixelRatio=" << screen->devicePixelRatio()
             << screen;


    auto mywidget = new MyWidget();
    mywidget->show();
    if (ishdpi) {
        mywidget->setWindowTitle("HDPI");
    } else {
        mywidget->setWindowTitle("NON-HDPI");
    }


    return app.exec();
}
