/**
 * Illustrates a crash when deleting a buddy from a label, in Qt < 5.11
 * QTBUG-66841
 */
#include <QtWidgets>

int main(int a, char **b)
{
    QApplication app(a, b);

    QWidget w;
    w.resize(500, 500);
    auto lay = new QVBoxLayout(&w);
    auto le = new QLineEdit();
    auto la = new QLabel("&Test");
    la->setBuddy(le);
    lay->addWidget(la);
    lay->addWidget(le);

    auto but = new QPushButton("Delete buddy");
    lay->addWidget(but);
    QObject::connect(but, &QPushButton::clicked, le, [le] {
        delete le;
        qDebug() << "Deleted line edit. Now use the shortcut alt+t.";
    });

    w.show();

    return app.exec();
}
