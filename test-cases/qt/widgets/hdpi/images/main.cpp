#include <QtWidgets>
#include <QDebug>



int main (int a, char **b)
{
    QApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QApplication::setAttribute(Qt::AA_UseHighDpiPixmaps);
    QApplication app(a, b);
    //app.setStyle(QStyleFactory::create("Fusion"));

    QPushButton but("");

    QIcon icon;
    icon.addFile(":/one.png", QSize(24, 24), QIcon::Normal);
    /*icon.addFile(":/one.png", QSize(24, 24), QIcon::Active);
    icon.addFile(":/one.png", QSize(24, 24), QIcon::Selected);
    icon.addFile(":/one.png", QSize(24, 24), QIcon::Disabled);*/

    but.setIcon(icon);
    but.show();


    qDebug() << "device pixel ratio" << app.devicePixelRatio() // 2
             << "sizes" << icon.availableSizes() // (24x24, 24x24)
             << "but dpr" << but.devicePixelRatio()
             << "window dpr=" << but.windowHandle()->devicePixelRatio();


    qDebug() << icon.pixmap(24, 24).devicePixelRatio()
             << icon.pixmap(but.windowHandle(), QSize(24, 24)).devicePixelRatio();


    return app.exec();
}

