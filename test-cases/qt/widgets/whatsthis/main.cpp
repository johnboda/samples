#include <QtWidgets>
#include <QDebug>

int main (int a, char **b)
{
    QApplication app(a, b);
    QWidget w;

    auto lay = new QVBoxLayout(&w);
    auto parent1 = new QWidget();
    auto lay1 = new QVBoxLayout(parent1);
    auto dialog = new QDialog();
    lay->addWidget(parent1);
    lay1->addWidget(dialog);
    
    w.resize(600, 600);
    w.show();

    return app.exec();
}
