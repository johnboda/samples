#include <QtWidgets>
#include <QDebug>

class MyWidget : public QWidget
{
public:
    explicit MyWidget(QWidget *parent = nullptr) : QWidget(parent)
    {
        qApp->installEventFilter(this);
    }

    bool event(QEvent *e) override { return QWidget::event(e); }
    void resizeEvent(QResizeEvent *e) override { return QWidget::resizeEvent(e); }
    void paintEvent(QPaintEvent *e) override { return QWidget::paintEvent(e); }
    void mouseMoveEvent(QMouseEvent *e) override { return QWidget::mouseMoveEvent(e); }
    bool eventFilter(QObject *, QEvent *)  override
    {
        return false;
    }
};

void dump(QWidget *w)
{
    qDebug() << "min size=" << w->minimumSize()
             << "; min size hint=" << w->minimumSizeHint()
             << "; max size=" << w->maximumSize()
             << "; size hint=" << w->sizeHint();
}

int main (int a, char **b)
{
    qputenv("QT_SCREEN_SCALE_FACTORS", "");
    qputenv("QT_AUTO_SCREEN_SCALE_FACTOR", "");

    QApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QApplication app(a, b);

    QWidget w;

    auto lay = new QVBoxLayout(&w);
    auto b1 = new QPushButton("Set style");
    auto b2 = new QPushButton("Remove style");

    lay->addWidget(b1);
    lay->addWidget(b2);

    auto l1 = new QLabel("label 1");
    auto l2 = new QLabel("label 2");
    auto l3 = new QLabel("label 3");

    lay->addWidget(l1);
    lay->addWidget(l2);
    lay->addWidget(l3);

    b1->connect(b1, &QPushButton::clicked, [l1, l2, l3, b1] {
        qApp->setStyleSheet("QWidget { font-size: 30pt; }");
        l1->setStyleSheet("QWidget { font-size: 30px; } ");
        l3->setStyleSheet("QWidget { font-size: 30pt; } ");

        b1->setStyleSheet("QWidget { min-height: 2em; max-height: 2em; } ");

    });


    b2->connect(b2, &QPushButton::clicked, [l1, l2, l3] {
        l1->setStyleSheet({});
        l2->setStyleSheet({});
        l3->setStyleSheet({});
    });

    w.resize(600, 600);
    w.show();

    return app.exec();
}
