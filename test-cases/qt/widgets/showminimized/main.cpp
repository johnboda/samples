#include <QtWidgets>
#include <QDebug>

class MyWidget : public QWidget
{
public:
    explicit MyWidget(QWidget *parent = nullptr) : QWidget(parent)
    {
        
    }

};

int main (int a, char **b)
{
    QApplication app(a, b);
    QPushButton w;
    w.resize(600, 600);
    w.show();
    QObject::connect(&w, &QPushButton::clicked, [&w] {
        w.showMinimized();
    });

    QWidget w2;
    w2.resize(100, 100);
    w2.show();

    return app.exec();
}
