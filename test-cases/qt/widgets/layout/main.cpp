#include <QtWidgets>
#include <QDebug>

class MyWidget : public QWidget
{
public:
    explicit MyWidget(QWidget *parent = nullptr) : QWidget(parent)
    {
        qApp->installEventFilter(this);
    }

    bool event(QEvent *e) override {

        if (e->type() == QEvent::LayoutRequest) {
            qDebug() << "LayoutRequest";
        }

        return QWidget::event(e); }
    void resizeEvent(QResizeEvent *e) override { return QWidget::resizeEvent(e); }
    void paintEvent(QPaintEvent *e) override { return QWidget::paintEvent(e); }
    void mouseMoveEvent(QMouseEvent *e) override { return QWidget::mouseMoveEvent(e); }
    bool eventFilter(QObject *, QEvent *)  override
    {
        return false;
    }

};

int main (int a, char **b)
{
    QApplication app(a, b);
    MyWidget w;
    w.resize(600, 600);
    w.show();

    auto lay = new QVBoxLayout(&w);
    auto child = new QWidget();
    lay->addWidget(child);
    qDebug() << "lay->children=" << lay->count();


    QPushButton but("Delete child");
    but.connect(&but, &QPushButton::clicked, [&w, lay, child] {
        delete child;
        qDebug() << "lay->children=" << lay->count();
    });
    but.show();

    return app.exec();
}
