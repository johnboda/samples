#include <QtWidgets>
#include <QDebug>

class MyWidget : public QWidget
{
public:
    explicit MyWidget(QWidget *parent = nullptr) : QWidget(parent)
    {
        qApp->installEventFilter(this);
    }

    bool event(QEvent *e) override { return QWidget::event(e); }
    void resizeEvent(QResizeEvent *e) override { return QWidget::resizeEvent(e); }
    void paintEvent(QPaintEvent *e) override { return QWidget::paintEvent(e); }
    void mouseMoveEvent(QMouseEvent *e) override { return QWidget::mouseMoveEvent(e); }
    bool eventFilter(QObject *, QEvent *)  override
    {
        return false;
    }
};

void dump(QWidget *w)
{
    qDebug() << "min size=" << w->minimumSize()
             << "; min size hint=" << w->minimumSizeHint()
             << "; max size=" << w->maximumSize()
             << "; size hint=" << w->sizeHint();
}

int main (int a, char **b)
{
    QApplication app(a, b);
    QWidget w;

    QWidget target;
    target.resize(500, 500);
    auto lay1 = new QVBoxLayout(&target);
    auto testButton = new QPushButton("TestButton");
    lay1->addWidget(testButton);

    auto lay = new QVBoxLayout(&w);
    auto b1 = new QPushButton("Set style1");
    auto b2 = new QPushButton("Set style2");
    auto b3 = new QPushButton("Set style3");
    auto b4 = new QPushButton("Remove style");
    lay->addWidget(b1);
    lay->addWidget(b2);
    lay->addWidget(b3);
    lay->addWidget(b4);

    b1->connect(b1, &QPushButton::clicked, [&w, testButton] {
        testButton->setStyleSheet("QWidget { min-height: 50; max-height: 50;} ");
        dump(testButton);
    });

    b2->connect(b2, &QPushButton::clicked, [&w, testButton] {
        testButton->setStyleSheet("QWidget { background: red; min-height: 100; max-height: 100; } ");
        dump(testButton);
    });

    b3->connect(b3, &QPushButton::clicked, [&w, testButton] {
        testButton->setStyleSheet("QWidget { background: green; } ");
        dump(testButton);
    });

    b4->connect(b4, &QPushButton::clicked, [&w, testButton] {
        testButton->setStyleSheet(QString());
        dump(testButton);
    });

    w.resize(600, 600);
    w.show();
    target.show();

    return app.exec();
}
