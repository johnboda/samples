#include <QtWidgets>
#include <QDebug>

class MyWidget : public QWidget
{
    Q_OBJECT
public:
    explicit MyWidget(QWidget *parent = nullptr)
        : QWidget(parent)
    {
        QTimer::singleShot(0, this, SLOT(startUpdate()));
    }
public Q_SLOTS:
    void startUpdate()
    {
        qDebug() << "app.quit()";
        QCoreApplication::quit();
    }
};

int main (int a, char **b)
{
    QApplication app(a, b);
    MyWidget w;


    qDebug() << "app.exec()";
    return app.exec();
}

#include <main.moc>
