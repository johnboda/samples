#include <QApplication>
#include <QOpenGLWidget>
#include <QPainter>
#include <QFile>
#include <QWindow>
#include <QSurfaceFormat>
#include <QDebug>


Q_DECLARE_METATYPE(QMargins)


class MyOpenGLWidget : public QOpenGLWidget
{
public:
    MyOpenGLWidget(QWidget* parent = nullptr) : QOpenGLWidget(parent) {
        auto f = format();
        f.setVersion(4, 1);
        // f.setRenderableType(QSurfaceFormat::OpenGLES);
        f.setProfile(QSurfaceFormat::NoProfile);
        setFormat(f);
    }

    void paintGL() override {
        QPainter painter(this);
        painter.fillRect(rect(), Qt::red);
        painter.setPen(Qt::white);
        painter.drawText(rect(), Qt::AlignCenter, "Qt");

        qDebug() << "version: " << format().version();
    }
};

int main(int argc, char** argv)
{
    QApplication app(argc, argv);

    MyOpenGLWidget w;
    w.resize(500, 500);
    w.show();



    return app.exec();
}
