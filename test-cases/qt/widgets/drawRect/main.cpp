#include <QtWidgets>
#include <QDebug>

#define PEN_WIDTH 1

class MyWidget : public QWidget
{
public:
    explicit MyWidget(QWidget *parent = nullptr) : QWidget(parent)
    {
        qApp->installEventFilter(this);
    }


    void paintEvent(QPaintEvent *) override
    {
        QPainter p(this);

        QPen pen(Qt::black);
        QRect r(0, 0, 100, 100);
        p.drawRect(r);

        p.translate(3, 3);
        p.drawRect(QRect(0,0,50,50));

        qDebug() << "width" << r.width();

    }

};

int main (int a, char **b)
{
    QApplication app(a, b);
    MyWidget w;
    w.resize(600, 600);
    w.show();


    return app.exec();
}
