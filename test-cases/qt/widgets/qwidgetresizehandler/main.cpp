// QT += widgets-private
#include <QtWidgets>
#include <QtWidgets/private/qwidgetresizehandler_p.h>

#if defined(Q_OS_WIN)
# include <Windowsx.h>
# pragma comment(lib,"User32.lib")
#endif
class MyWidget : public QWidget
{
public:
    MyWidget() {

        auto lay = new QVBoxLayout(this);
        lay->addWidget(new QPushButton("FOO"));
        lay->addWidget(new QPushButton("FOO"));
        lay->addWidget(new QPushButton("FOO"));
        lay->addWidget(new QPushButton("FOO"));
        lay->addWidget(new QPushButton("FOO"));
        lay->addWidget(new QPushButton("FOO"));
        lay->addWidget(new QPushButton("FOO"));
        lay->addWidget(new QPushButton("FOO"));
        lay->addWidget(new QPushButton("FOO"));
        lay->addWidget(new QPushButton("FOO"));

    }

#if defined(Q_OS_WIN)
    bool nativeEvent(const QByteArray &eventType, void *message, long *result) override
    {
        if (eventType != "windows_generic_MSG")
            return false;

        if (window() != this)
            return false;

        const int borderWidth = 8;
        const bool hasFixedWidth = minimumWidth() == maximumWidth();
        const bool hasFixedHeight = minimumHeight() == maximumHeight();

        auto msg = static_cast<MSG *>(message);
        if (msg->message == WM_NCCALCSIZE) {
            *result = 0;
            return true;
        } else if (msg->message == WM_NCHITTEST) {

            *result = 0;
            const int xPos = GET_X_LPARAM(msg->lParam);
            const int yPos = GET_Y_LPARAM(msg->lParam);
            RECT rect;
            GetWindowRect(reinterpret_cast<HWND>(winId()), &rect);

            if (xPos >= rect.left && xPos <= rect.left + borderWidth &&
                yPos <= rect.bottom && yPos >= rect.bottom - borderWidth) {
                *result = HTBOTTOMLEFT;
            } else if (xPos < rect.right && xPos >= rect.right - borderWidth &&
                yPos <= rect.bottom && yPos >= rect.bottom - borderWidth) {
                *result = HTBOTTOMRIGHT;
            } else if (xPos >= rect.left && xPos <= rect.left + borderWidth &&
                yPos >= rect.top && yPos <= rect.top + borderWidth) {
                *result = HTTOPLEFT;
            } else if (xPos <= rect.right && xPos >= rect.right - borderWidth &&
                yPos >= rect.top && yPos < rect.top + borderWidth) {
                *result = HTTOPRIGHT;
            } else if (!hasFixedWidth && xPos >= rect.left && xPos <= rect.left + borderWidth) {
                *result = HTLEFT;
            } else if (!hasFixedHeight && yPos >= rect.top && yPos <= rect.top + borderWidth) {
                *result = HTTOP;
            } else if (!hasFixedHeight && yPos <= rect.bottom && yPos >= rect.bottom - borderWidth) {
                *result = HTBOTTOM;
            } else if (!hasFixedWidth && xPos <= rect.right && xPos >= rect.right - borderWidth) {
                *result = HTRIGHT;
            }

            return *result != 0;
        }

        return false;
    }
#endif

    void set_borderless()
    {
        HWND hwnd = (HWND)winId();
        {
            LONG newStyle = ( WS_POPUP | WS_CAPTION | WS_THICKFRAME | WS_MAXIMIZEBOX | WS_MINIMIZEBOX);
            SetWindowLongPtr(hwnd, GWL_STYLE, static_cast<LONG>(newStyle));

            //redraw frame
            SetWindowPos(hwnd, 0, 0, 0, 0, 0, SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE);

            HWND hwnd = (HWND)winId();
            ShowWindow(hwnd, SW_SHOW);
        }
    }

};


void testQtResizeHandler()
{
    auto w = new QWidget();
    w->setWindowFlags(w->windowFlags() | Qt::FramelessWindowHint);
    new QWidgetResizeHandler(w); // the magic
    w->show();
}


void testWMHITTEST()
{
    auto w = new MyWidget();
    //w->setWindowFlags(w->windowFlags() | Qt::FramelessWindowHint);
    w->setWindowFlags(w->windowFlags() | Qt::Tool);
    w->show();
}

int main(int argc, char **argv)
{
    QApplication app(argc, argv);
    //testQtResizeHandler();
    testWMHITTEST();
    return app.exec();
}
