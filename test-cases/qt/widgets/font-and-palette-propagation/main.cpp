#include <QtWidgets>
#include <QDebug>


int main (int a, char **b)
{
    QApplication app(a, b);
    QWidget w;
    auto lay = new QVBoxLayout(&w);
    auto le = new QLineEdit(&w);
    auto te = new QTextEdit(&w);
    auto cb = new QCheckBox(&w);
    auto rb = new QRadioButton(&w);
    auto cb2 = new QComboBox(&w);

    QPalette palette = w.palette();
    palette.setColor(QPalette::Active, QPalette::Base, Qt::blue);
    palette.setColor(QPalette::Active, QPalette::Background, Qt::red);
    w.setPalette(palette);

    lay->addWidget(le);
    lay->addWidget(te);
    lay->addWidget(cb);
    lay->addWidget(rb);
    lay->addWidget(cb);
    lay->addWidget(cb2);
    w.resize(600, 600);
    w.show();

    return app.exec();
}


