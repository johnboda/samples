#include <QtWidgets>
#include <QDebug>

class MyWidget : public QWidget
{
public:
    explicit MyWidget(QWidget *parent = nullptr) : QWidget(parent)
    {
    }

    bool event(QEvent *e) override { return QWidget::event(e); }
    void resizeEvent(QResizeEvent *e) override { return QWidget::resizeEvent(e); }
    void paintEvent(QPaintEvent *e) override { return QWidget::paintEvent(e); }
    void mouseMoveEvent(QMouseEvent *e) override { return QWidget::mouseMoveEvent(e); }
};

int main (int a, char **b)
{
    QApplication app(a, b);
    MyWidget w;
    w.setToolTip("one two three four five six seven");
    w.resize(600, 600);
    w.show();



    return app.exec();
}
