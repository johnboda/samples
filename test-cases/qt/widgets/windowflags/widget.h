#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = nullptr);
    ~Widget();

    Qt::WindowFlags flags() const;
    void createWindow();
    bool event(QEvent *) override;

private:
    Ui::Widget *ui;
};

#endif

