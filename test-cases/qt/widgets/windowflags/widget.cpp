#include "widget.h"
#include "ui_widget.h"

#include <QMetaEnum>
#include <QDebug>
#include <QTimer>

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{

    QTimer::singleShot(0, [this] {
        qDebug() << testAttribute(Qt::WA_WState_Created);
    });
    //ui->setupUi(this);



    //connect(ui->pushButton, &QPushButton::clicked, this, &Widget::createWindow);
}

bool Widget::event(QEvent*e)
{
    qDebug() << e->type();


    if (QEvent::WinIdChange == e->type()) {
        delete this; delete this;
    }

    return false;
}

Widget::~Widget()
{
    delete ui;
}

Qt::WindowFlags Widget::flags() const
{
    Qt::WindowFlags f = 0;
    auto checkboxes = findChildren<QCheckBox*>();

    for (auto checkbox : checkboxes) {
        if (!checkbox->isChecked())
            continue;

        QString enumKey = QStringLiteral("Qt::%1").arg(checkbox->text());
        int value = QMetaEnum::fromType<Qt::WindowType>().keyToValue(enumKey.toLatin1().constData());
        if (value < 0) {
            qFatal(QString("Invalid enumerator: %1").arg(checkbox->text()).toLatin1().constData());
        }

        f |= static_cast<Qt::WindowType>(value);
        qDebug() << "Adding" << checkbox->text() << "; value=" << QString::number(value, 16);

    }

    return f;
}

void Widget::createWindow()
{
    Qt::WindowFlags f = flags();

    QWidget *w = new QWidget(nullptr, f);
    w->setAttribute(Qt::WA_DeleteOnClose);
    w->show();
    w->resize(500, 500);
}
