#include <QtWidgets>

int main(int a, char **b)
{
    QApplication app(a, b);

    QWidget w;
    w.setWindowFlag(Qt::FramelessWindowHint, true);

    auto b1 = new QPushButton("System Resize");
    auto b2 = new QPushButton("System Move");

    auto lay = new QVBoxLayout(&w);
    lay->addWidget(b1);
    lay->addWidget(b2);
    lay->addStretch();

    w.show();

    QObject::connect(b1, &QPushButton::clicked, [&w] {
        w.windowHandle()->startSystemResize(Qt::RightEdge);
    });

    QObject::connect(b2, &QPushButton::clicked, [&w] {
        w.windowHandle()->startSystemMove();
    });

    return app.exec();
}

