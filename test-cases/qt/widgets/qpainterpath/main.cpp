#include <QtWidgets>
#include <QDebug>

class MyWidget : public QWidget
{
public:
    explicit MyWidget(QWidget *parent = nullptr) : QWidget(parent)
    {

    }


    void paintEvent(QPaintEvent *) override {
        QPainter p(this);
        QRect r = QRect(100, 100, 30, 100);
        p.setRenderHint(QPainter::Antialiasing);
        r.moveCenter(r.center());
        p.drawRoundedRect(r, 3, 3);
    }


};

int main (int a, char **b)
{
    QApplication app(a, b);
    MyWidget w;
    w.resize(600, 600);
    w.show();

    QPushButton but("Dump Debug");
    but.connect(&but, &QPushButton::clicked, [&w] {

    });
    but.show();

    return app.exec();
}
