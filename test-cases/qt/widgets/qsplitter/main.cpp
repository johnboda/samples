#include <QtWidgets>
#include <QDebug>

class MyWidget : public QWidget
{
public:
    explicit MyWidget(QWidget *parent = nullptr) : QWidget(parent)
    {
        qApp->installEventFilter(this);
    }

    bool event(QEvent *e) override { return QWidget::event(e); }
    void resizeEvent(QResizeEvent *e) override { return QWidget::resizeEvent(e); }
    void paintEvent(QPaintEvent *e) override { return QWidget::paintEvent(e); }
    void mouseMoveEvent(QMouseEvent *e) override { return QWidget::mouseMoveEvent(e); }
    bool eventFilter(QObject *, QEvent *)  override
    {
        return false;
    }
};

int main (int a, char **b)
{
    QApplication app(a, b);
    QPushButton w1("one");
    QPushButton w2("two");

    QSplitter splitter;
    splitter.resize(600, 600);
    splitter.show();

    splitter.addWidget(&w1);
    splitter.addWidget(&w2);


    QPushButton but("Resize button 1");
    but.connect(&but, &QPushButton::clicked, [&w1] {
        w1.resize(100, 100);
    });
    but.show();

    return app.exec();
}
