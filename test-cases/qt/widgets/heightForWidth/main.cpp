#include <QtWidgets>
#include <QDebug>

class PushButton : public QPushButton
{
public:
    explicit PushButton(const QString &text, QWidget *p) : QPushButton(text, p)
    {

    }

    int heightForWidth(int w) const override
    {
        return w;
    }

};

class Grid : public QGridLayout {
public:
    int heightForWidth(int w) const override { return w; }
};

class MyWidget : public QWidget
{
public:
    explicit MyWidget(QWidget *parent = nullptr) : QWidget(parent)
    {
        auto layout = new Grid();
        setLayout(layout);
        auto frame = new PushButton("test", this);

        QSizePolicy policy(QSizePolicy::Preferred, QSizePolicy::Maximum);
        policy.setHeightForWidth(true);
        frame->setSizePolicy(policy);

        layout->addWidget(frame, 0, 0);


    }

    bool event(QEvent *e) override { return QWidget::event(e); }
    void resizeEvent(QResizeEvent *e) override { return QWidget::resizeEvent(e); }
    void paintEvent(QPaintEvent *e) override { return QWidget::paintEvent(e); }
    void mouseMoveEvent(QMouseEvent *e) override { return QWidget::mouseMoveEvent(e); }
    bool eventFilter(QObject *, QEvent *)  override
    {
        return false;
    }
};

int main (int a, char **b)
{
    QApplication app(a, b);
    MyWidget w;
    w.resize(600, 600);
    w.show();

    QPushButton but("Dump Debug");
    but.connect(&but, &QPushButton::clicked, [&w] {

    });
    but.show();

    return app.exec();
}
