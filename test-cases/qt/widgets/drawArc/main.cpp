#include <QtWidgets>
#include <QDebug>

#define PEN_WIDTH 1

class MyWidget : public QWidget
{
public:
    explicit MyWidget(QWidget *parent = nullptr) : QWidget(parent)
    {
        qApp->installEventFilter(this);
    }

    void drawThickArc(QPainter &p)
    {
        const QRect r = rect();
        const QRect r2 = rect().adjusted(20, 20, -20, -20);

        QPainterPath path;
        path.moveTo(QPoint(r.width() - 1, r.height() / 2));

        int startAngle = 0 ;
        int span = 90;
        path.arcTo(r, startAngle, span);
        path.arcTo(r2, startAngle + span, -span); // go backwards now
        path.closeSubpath();

        p.fillPath(path, Qt::red);
        p.setPen(Qt::black);
        p.drawPath(path);
    }

    void drawArc(QPainter &p)
    {
        QPen pen(Qt::black);

        int startAngle = 0 * 16;
        int span = 90 * 16;

        pen.setWidth(PEN_WIDTH);
        p.setPen(pen);
        p.drawArc(rect(), startAngle, span);
    }

    void paintEvent(QPaintEvent *) override
    {
        QPainter p(this);
        p.setRenderHint(QPainter::Antialiasing);

        drawThickArc(p);
        //drawArc(p);
    }
};

int main (int a, char **b)
{
    QApplication app(a, b);
    MyWidget w;
    w.resize(600, 600);
    w.show();


    return app.exec();
}
