#include <QtWidgets>
#include <QDebug>

class MyWidget : public QWidget
{
public:
    explicit MyWidget(QWidget *parent = nullptr) : QWidget(parent)
    {
        m_lastPaintEvent.start();
    }

    bool event(QEvent *e) override { return QWidget::event(e); }
    void resizeEvent(QResizeEvent *e) override { return QWidget::resizeEvent(e); }
    void paintEvent(QPaintEvent *e) override
    {
        // Linux: around 8ms. Others untested
        qDebug() << "Time since last paint" << m_lastPaintEvent.elapsed();
        m_lastPaintEvent.start();
        return QWidget::paintEvent(e);
    }

    QTime m_lastPaintEvent;
};

int main (int a, char **b)
{
    QApplication app(a, b);
    MyWidget w;
    w.resize(600, 600);
    w.show();
    return app.exec();
}
