#ifndef MYSCENE_H
#define MYSCENE_H

#include <QGraphicsScene>
#include <QGraphicsSceneMouseEvent>
#include <QGraphicsRectItem>
#include <QGraphicsDropShadowEffect>

class MyRect : public QGraphicsRectItem
{
public:
    MyRect(QGraphicsItem * parent = nullptr)
        : QGraphicsRectItem(QRectF{0.0f, 0.0f, 100.0f, 100.0f}, parent)
    {
        setPen(QPen(Qt::white));
        setBrush(QBrush(Qt::white, Qt::SolidPattern));
    }
};

class MyScene: public QGraphicsScene
{
    Q_OBJECT

public:
    MyScene(QObject * parent = Q_NULLPTR)
        : QGraphicsScene(parent), background(0.0f, 0.0f, 500.0f, 500.0f), global_rect(Q_NULLPTR)
    {
        background.setPen(QPen(Qt::white));
        background.setBrush(QBrush(Qt::red, Qt::SolidPattern));
        addItem(&background);

        QGraphicsDropShadowEffect * shadow=new QGraphicsDropShadowEffect();
        shadow->setBlurRadius(15.0f);

        global_rect.setGraphicsEffect(shadow);


        addItem(new QGraphicsRectItem(&global_rect));

    }

    void mouseReleaseEvent(QGraphicsSceneMouseEvent* me) override
    {
        if (me->button()==Qt::LeftButton)
        {
            addItem(&global_rect);
            global_rect.setPos(me->scenePos());
        }
        else if (me->button()==Qt::RightButton)
        {
            removeItem(&global_rect);
        }

        QGraphicsScene::mouseReleaseEvent(me);
    }

    QGraphicsRectItem background;
    MyRect global_rect;
};

#endif // MYSCENE_H