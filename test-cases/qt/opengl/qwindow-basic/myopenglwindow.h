#ifndef MYOPENGLWINDOW_H
#define MYOPENGLWINDOW_H

class QOpenGLContext;
class QOpenGLPaintDevice;

#include <QWindow>
#include <QOpenGLFunctions>
#include <QTime>

class MyOpenGLWindow : public QWindow, protected QOpenGLFunctions
{
    Q_OBJECT
public:
    explicit MyOpenGLWindow(QWindow *parent = nullptr);
    ~MyOpenGLWindow();

    virtual void render(QPainter *painter);
    virtual void render();

    virtual void initialize() = 0;

    void setAnimating(bool animating);

public slots:
    void renderLater();
    void renderNow();

protected:
    bool event(QEvent *event) override;
    void exposeEvent(QExposeEvent *event) override;

private:
    bool m_animating;

    QOpenGLContext *m_context;
    QOpenGLPaintDevice *m_device;
    QTime t;
};

#endif
