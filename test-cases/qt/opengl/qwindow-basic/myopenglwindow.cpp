#include "myopenglwindow.h"

#include <QPainter>
#include <QOpenGLPaintDevice>

#include <QTime>
#include <QDebug>
#include <QTimer>

MyOpenGLWindow::MyOpenGLWindow(QWindow *parent)
    : QWindow(parent)
    , m_animating(true)
    , m_context(nullptr)
    , m_device(nullptr)
{
    setSurfaceType(QWindow::OpenGLSurface);
    auto f = format();
    f.setSwapInterval(0);
    setFormat(f);
    t.start();
}

MyOpenGLWindow::~MyOpenGLWindow()
{
}

void MyOpenGLWindow::render(QPainter *painter)
{
    Q_UNUSED(painter);
    painter->fillRect(QRect(0, 0, 100, 100), Qt::blue);
}

void MyOpenGLWindow::render()
{
    if (!m_device)
        m_device = new QOpenGLPaintDevice;

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

    m_device->setSize(size());

    QPainter painter(m_device);
    render(&painter);
}

void MyOpenGLWindow::renderLater()
{
    requestUpdate();
    //QTimer::singleShot(0, this, &MyOpenGLWindow::renderNow);
}

bool MyOpenGLWindow::event(QEvent *event)
{
    switch (event->type()) {
    case QEvent::UpdateRequest:
        renderNow();
        return true;
    default:
        return QWindow::event(event);
    }
}

void MyOpenGLWindow::exposeEvent(QExposeEvent *event)
{
    Q_UNUSED(event);

    if (isExposed())
        renderNow();
}

void MyOpenGLWindow::renderNow()
{
    if (!isExposed())
        return;

    qDebug() << "Elapsed" << t.elapsed();
    t.start();

    bool needsInitialize = false;

    if (!m_context) {
        m_context = new QOpenGLContext(this);
        m_context->setFormat(requestedFormat());
        m_context->create();

        needsInitialize = true;
    }

    m_context->makeCurrent(this);

    if (needsInitialize) {
        initializeOpenGLFunctions();
        initialize();
    }

    render();

    m_context->swapBuffers(this);

    if (m_animating)
        renderLater();
}

void MyOpenGLWindow::setAnimating(bool animating)
{
    m_animating = animating;

    if (animating)
        renderLater();
}
