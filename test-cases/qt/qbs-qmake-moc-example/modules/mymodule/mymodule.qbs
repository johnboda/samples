import qbs 1.0

Module {

    Rule {
        inputs: ["cpp"]
        Artifact {
            filePath: input.fileName + ".out"
            fileTags: ["moc"]
        }

        prepare: {
            var cmd = new Command("moc", [input.filePath, '-o', 'foo']);
            cmd.highlight = 'codegen'
            cmd.description = 'running moc ' + input.filePath
            return [cmd];
        }
    }
}
