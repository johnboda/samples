#include <QtCore>

/**
 * @brief Just a benchmark to show that storing QUuid in QHash is much faster for lookups than
 * QString.
 */


template <typename C, typename Id>
int benchmark(const C &container, const QVector<Id> &ids, const QString &description)
{
    QElapsedTimer t;
    t.start();

    const int numIds = ids.size();
    int result = 0;
    for (int i = 0; i < 100000000; ++i)
        result += container.value(ids.at(i % numIds));

    qDebug() << description << t.elapsed();
    return result;
}

int main(int a, char **b)
{
    QCoreApplication app(a, b);

    QVector<QUuid> uuids;
    QVector<QString> stringIds;

    QHash<QUuid, int> hash;
    QMap<QUuid, int> map;
    const int numIds = 1000;
    uuids.reserve(numIds);
    stringIds.reserve(numIds);

    for (int i = 0; i < numIds; ++i) {
        QUuid uuid = QUuid::createUuid();
        uuids << uuid;
        stringIds << uuid.toString();
        hash.insert(uuid, i);
        map.insert(uuid, i);
    }

    int result = 0;

    const QStringList arguments = qApp->arguments();

    result += benchmark(hash, uuids, QStringLiteral("QUuid lookups:"));
    result += benchmark(map, stringIds, QStringLiteral("QString lookups:"));

    return result;
}
