import 'package:console_full_hello_world/console_full_hello_world.dart' as console_full_hello_world;

void main(List<String> arguments) {
  print('Hello world: ${console_full_hello_world.calculate()}!');
}
