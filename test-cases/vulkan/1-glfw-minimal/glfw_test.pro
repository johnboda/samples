TEMPLATE = app
TARGET = glfw_test
INCLUDEPATH += .

# Input
SOURCES += main.cpp
QT -= core
LIBS += -lvulkan -lglfw
