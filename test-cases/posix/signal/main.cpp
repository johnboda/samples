
#include <QCoreApplication>
#include <QThread>
#include <QDebug>

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>

#include <QTimer>

#define OPTION 3
#define TEST_IF_THREAD_STOPS_WHEN_HANDLER false

void signal_handler(int signum)
{
    printf("Caught signal %d\n", signum);

    qDebug() << "Current threadID = " << QThread::currentThreadId() << "; isMain=" << (qApp->thread() == QThread::currentThread());

    if (SLEEP_IN_HANDLER)
        sleep(300000);

}

void setup_with_sigaction()
{
    struct sigaction sa;

    memset(&sa, 0, sizeof(struct sigaction));
    sa.sa_handler = &signal_handler;
    if (sigaction(SIGINT, &sa, NULL) == -1) {
        perror("sigaction");
        return;
    }

}

void setup_with_signal()
{
    signal(SIGINT, signal_handler); // Catch Ctrl+C
}


class MyThread : public QThread
{
public:
    void run() override
    {

        setup_with_sigaction();
        while (TEST_IF_THREAD_STOPS_WHEN_HANDLER) {
            static int count = 0;
            count++;
            qDebug() << "timer in the thread!" << count; // This stops printing when signal handler executes
        }


        exec();
    }
};

void setup_in_thread()
{
    auto thread = new MyThread();
    thread->run();
}

int main(int a, char**b)
{
    QCoreApplication app(a, b);

    if (OPTION == 1) {
        setup_with_sigaction();
    } else if (OPTION == 2) {
        setup_with_signal();
    } else if (OPTION == 3) {
        setup_in_thread();
    }



    return app.exec();
}

