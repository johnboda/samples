
    void dumpWidgetDebug(QWidget *w, int indent = 0)
    {
        QString s(4*indent, ' ');
        qDebug() << s << "-" << w << "; WA_stylesheet=" << w->testAttribute(Qt::WA_StyleSheet)
                 << "; polished=" << w->testAttribute(Qt::WA_WState_Polished)
                 << "; sheet=" << w->styleSheet().size()
                 << "; style=" << w->style()
                 << "; WA_setStyle" << w->testAttribute(Qt::WA_SetStyle);


        for (auto c : w->children()) {
            if (auto childWidget = qobject_cast<QWidget*>(c)) {
                dumpWidgetDebug(childWidget, indent+1);
            }
        }
    }
